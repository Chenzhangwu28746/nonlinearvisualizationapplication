-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: nonlinearvisualization
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `operation`
--

DROP TABLE IF EXISTS `operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `operation` (
  `id` bigint(20) NOT NULL,
  `operation_id` bigint(20) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  `operation_order` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `first_grammar` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `second_grammar` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `third_grammar` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `fourth_grammar` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation`
--

LOCK TABLES `operation` WRITE;
/*!40000 ALTER TABLE `operation` DISABLE KEYS */;
INSERT INTO `operation` VALUES (1,1,4,'019','topoa','cam','topoa','cam'),(2,2,4,'013','cam','topoa','cam','topoa'),(3,3,4,'011','cam','topoa','cam','topoc'),(4,4,4,'009','topoa','cam','topoc','cam'),(5,5,4,'008','topoa','topoa','cam','topoa'),(6,6,4,'007','cam','topoc','cam','topoa'),(7,7,3,'006','cam','topoa','topoa','cam'),(8,8,3,'005','topoc','cam','cam','topoa'),(9,9,3,'005','topoa','topoa','cam','topoa'),(10,10,3,'003','cam','topoc','topoa','cam'),(11,11,3,'003','topoc','topoa','cam','topoa'),(12,12,3,'003','cam','topoc','cam','topoc'),(13,13,3,'003','topoc','cam','topoc','cam'),(14,14,3,'002','topoc','topoa','topoa','cam'),(15,15,3,'002','topoa','topoa','cam','transform'),(16,16,3,'002','topoa','cam','topoc','topoa'),(17,17,3,'002','topoc','cam','topoa','topoa'),(18,18,3,'001','cam','topoa','topoc','topoa'),(19,19,3,'001','topoa','topoc','cam','topoa'),(20,20,3,'001','topoc','cam','topoa','topoc');
/*!40000 ALTER TABLE `operation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-25 11:12:53
