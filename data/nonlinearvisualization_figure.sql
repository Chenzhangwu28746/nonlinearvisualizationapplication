-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: nonlinearvisualization
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `figure`
--

DROP TABLE IF EXISTS `figure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `figure` (
  `id` bigint(20) NOT NULL,
  `figure_id` bigint(20) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  `figure_order` bigint(20) DEFAULT NULL,
  `figure_name` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `figur_info` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `figure_address` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `figure`
--

LOCK TABLES `figure` WRITE;
/*!40000 ALTER TABLE `figure` DISABLE KEYS */;
INSERT INTO `figure` VALUES (1,1,3,1,'001.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\1.jpg'),(2,2,3,2,'002.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\2.jpg'),(3,3,3,3,'003.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\3.jpg'),(4,4,3,4,'004.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\4.jpg'),(5,5,3,5,'005.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\5.jpg'),(6,6,3,6,'006.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\6.jpg'),(7,7,3,7,'007.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\7.jpg'),(8,8,3,8,'008.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\8.jpg'),(9,9,3,9,'009.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\9.jpg'),(10,10,3,10,'010.jpg','jpg','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\figure\\4\\10.jpg'),(11,11,3,11,'011.jpg','jpg',NULL),(12,12,3,12,'012.jpg','jpg',NULL),(13,13,3,13,'013.jpg','jpg',NULL),(14,14,3,14,'014.jpg','jpg',NULL),(15,15,3,15,'015.jpg','jpg',NULL),(16,16,3,16,'016.jpg','jpg',NULL),(17,17,3,17,'017.jpg','jpg',NULL),(18,18,3,18,'018.jpg','jpg',NULL),(19,19,3,19,'019.jpg','jpg',NULL),(20,20,3,20,'020.jpg','jpg',NULL),(21,21,3,21,'021.jpg',NULL,NULL),(22,22,3,22,'022.jpg',NULL,NULL),(23,23,3,23,'023.jpg',NULL,NULL),(24,24,3,24,'024.jpg',NULL,NULL),(25,25,3,25,'025.jpg',NULL,NULL),(26,26,3,26,'026.jpg',NULL,NULL),(27,27,3,27,'027.jpg',NULL,NULL),(28,28,3,28,'028.jpg',NULL,NULL),(29,29,3,29,'029.jpg',NULL,NULL),(30,30,3,30,'030.jpg',NULL,NULL),(31,31,3,31,'031.jpg',NULL,NULL),(32,32,3,32,'032.jpg',NULL,NULL),(33,33,3,33,'033.jpg',NULL,NULL),(34,34,3,34,'034.jpg',NULL,NULL),(35,35,3,35,'035.jpg',NULL,NULL),(36,36,3,36,'036.jpg',NULL,NULL),(37,37,3,37,'037.jpg',NULL,NULL),(38,38,3,38,'038.jpg',NULL,NULL),(39,39,3,39,'039.jpg',NULL,NULL),(40,40,3,40,'040.jpg',NULL,NULL),(41,41,3,41,'041.jpg',NULL,NULL),(42,42,3,42,'042.jpg',NULL,NULL),(43,43,3,43,'043.jpg',NULL,NULL),(44,44,3,44,'044.jpg',NULL,NULL),(45,45,3,45,'045.jpg',NULL,NULL),(46,46,3,46,'046.jpg',NULL,NULL),(47,47,3,47,'047.jpg',NULL,NULL),(48,48,3,48,'048.jpg',NULL,NULL),(49,49,3,49,'049.jpg',NULL,NULL),(50,50,3,50,'050.jpg',NULL,NULL),(51,51,3,51,'051.jpg',NULL,NULL),(52,52,3,52,'052.jpg',NULL,NULL),(53,53,3,53,'053.jpg',NULL,NULL),(54,54,3,54,'054.jpg',NULL,NULL),(55,55,3,55,'055.jpg',NULL,NULL),(56,56,3,56,'056.jpg',NULL,NULL),(57,57,3,57,'057.jpg',NULL,NULL),(58,58,3,58,'058.jpg',NULL,NULL),(59,59,3,59,'059.jpg',NULL,NULL);
/*!40000 ALTER TABLE `figure` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-25 11:12:52
