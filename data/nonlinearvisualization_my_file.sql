-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: nonlinearvisualization
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `my_file`
--

DROP TABLE IF EXISTS `my_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `my_file` (
  `id` bigint(20) NOT NULL,
  `my_file_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `my_file_name` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `my_file_info` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  `my_file_address` varchar(450) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_file`
--

LOCK TABLES `my_file` WRITE;
/*!40000 ALTER TABLE `my_file` DISABLE KEYS */;
INSERT INTO `my_file` VALUES (98678945880211464,98678945880211465,1,'文件1.docx','这是文件1.docx','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\4\\文件1.docx'),(98678945880211466,98678945880211467,2,'文件2.pdf','文件2.pdf','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\4\\文件2.pdf'),(98678945880211476,98678945880211477,NULL,'文件1.docx','文件1.docx','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\null\\文件1.docx'),(98678945880211478,98678945880211479,NULL,'概率.docx','概率.docx','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\null\\概率.docx'),(98678945880211480,98678945880211481,NULL,'依赖.docx','依赖.docx','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\null\\依赖.docx'),(98678945880211498,98678945880211499,1,'依赖.docx','依赖.docx','C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\1\\依赖.docx');
/*!40000 ALTER TABLE `my_file` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-25 11:12:52
