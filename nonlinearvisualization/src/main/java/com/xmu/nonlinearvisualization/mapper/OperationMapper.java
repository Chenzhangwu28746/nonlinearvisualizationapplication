package com.xmu.nonlinearvisualization.mapper;


import com.xmu.nonlinearvisualization.entity.Operation;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与操作相关的mapper
 *
 * @author ahua
 * @date 2020/1/28
 */

@Repository
public interface OperationMapper {
    /**
     * 根据版本id得到所有操作
     *
     * @param versionId
     * @return List<Operation>
     */
    public List<Operation> getAllOperationByVersionId(Long versionId);
}
