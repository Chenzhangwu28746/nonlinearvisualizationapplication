package com.xmu.nonlinearvisualization.mapper;

import com.xmu.nonlinearvisualization.entity.Prototype;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 与prototype相关的mapper
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public interface PrototypeMapper {
    /**
     * fetch user
     *
     * @param prototypeId
     * @return a user
     */
    List<Prototype> selectPrototypeById(Long prototypeId);


}
