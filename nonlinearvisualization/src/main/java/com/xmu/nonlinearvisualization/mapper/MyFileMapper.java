package com.xmu.nonlinearvisualization.mapper;


import com.xmu.nonlinearvisualization.entity.MyFile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与文件相关的mapper
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public interface MyFileMapper {

    /**
     * 插入文件
     *
     * @param myFile
     * @return void
     */
    public void insertNewFile(MyFile myFile);

    /**
     * 计算相同文件名数
     *
     * @param fileName
     * @return int
     */
    public int countSameFile(String fileName);


    /**
     * 根据版本id得到所有文件信息
     *
     * @param curProjectId
     * @return List<MyFile>
     */
    public List<MyFile> getAllFilesByCurProjectId(Long curProjectId);


    /**
     * 根据文件id删除其
     *
     * @param myFileId
     * @return void
     */
    public void deleteOneFileById(Long myFileId);

    /**
     * 根据文件id找到其位置
     *
     * @param myFileId
     * @return String
     */
    public String findFileAddressByMyFileId(Long myFileId);

    /**
     * 根据文件id找到其名字
     *
     * @param myFileId
     * @return String
     */
    public String findOneFileNameByFileId(Long myFileId);

    /**
     * 根据文件id找到其信息
     *
     * @param myFileId
     * @return String
     */
    public String findOneFileInfoByFileId(Long myFileId);


    /**
     * 修改文件信息
     *
     * @param myFile
     * @return
     */
    public void modifyFileInfo(MyFile myFile);
}
