package com.xmu.nonlinearvisualization.mapper;


import com.xmu.nonlinearvisualization.entity.Figure;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与轮播图片相关的mapper
 *
 * @author ahua
 * @date 2020/1/28
 */
@Repository
public interface FigureMapper {

    /**
     * 根据版本id得到所有轮播图片
     *
     * @param versionId
     * @return List<Figure>
     */
    public List<Figure> getAllFigureByVersionId(Long versionId);

}
