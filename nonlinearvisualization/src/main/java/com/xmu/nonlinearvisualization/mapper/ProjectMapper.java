package com.xmu.nonlinearvisualization.mapper;

import com.xmu.nonlinearvisualization.entity.Project;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与项目相关的mapper
 *
 * @author ahua
 * @date 2020/4/18
 */

@Repository
public interface ProjectMapper {

    /**
     * 根据版本id得到所有操作
     *
     * @param account
     * @return List<Project>
     */
    public List<Project> selectAllProjectByAccount(Long account);

    /**
     * 根据项目id得到最新版本
     *
     * @param projectId
     * @return Long
     */
    public Long getTheNewestVersionId(Long projectId);

    /**
     * 根据项目id得到最新版本
     *
     * @param projectId
     * @return Long
     */
    public Long getTheNewestVersionOrder(Long projectId);


    /**
     * 根据项目id得到版本数
     *
     * @param projectId
     * @return Long
     */
    public Long getVersionNumByProjectId(Long projectId);


    /**
     * 更新项目信息
     *
     * @param project
     * @return void
     */
    public void updateTheNewestVersion(Project project);

    /**
     * 添加新项目
     *
     * @param project
     * @return void
     */
    public void addNewProject(Project project);

    /**
     * 修改项目名称
     *
     * @param project
     * @return void
     */
    public void modifyProjectName(Project project);

    /**
     * 修改项目信息
     *
     * @param project
     * @return void
     */
    public void modifyProjectInfo(Project project);

}
