package com.xmu.nonlinearvisualization.mapper;

import com.xmu.nonlinearvisualization.entity.Designer;
import org.springframework.stereotype.Repository;

/**
 * 与设计者类别相关的mapper
 *
 * @author ahua
 * @date 2020/4/18
 */
@Repository
public interface DesignerMapper {

    /**
     * 根据版本account查询设计者
     *
     * @param designer
     * @return Designer
     */
    public int hasTheDesigner(Designer designer);
}
