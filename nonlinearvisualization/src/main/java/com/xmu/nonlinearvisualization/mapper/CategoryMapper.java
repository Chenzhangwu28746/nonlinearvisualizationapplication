package com.xmu.nonlinearvisualization.mapper;

import com.xmu.nonlinearvisualization.entity.Category;
import com.xmu.nonlinearvisualization.entity.Idea;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与想法类别相关的mapper
 *
 * @author ahua
 * @date 2020/1/14
 */
@Repository
public interface CategoryMapper {


    /**
     * 根据版本id查询所有想法及其类别
     *
     * @param projectId
     * @return List<Category>
     */
    public List<Category> selectCategoryListByProjectId(Long projectId);

    /**
     * 修改类别的名称
     *
     * @param category
     * @return void
     */
    public void modifyCategoryName(Category category);


    /**
     * 修改想法内容
     *
     * @param idea
     * @return void
     */
    public void modifyIdeaContent(Idea idea);


    /**
     * 删除某想法
     *
     * @param ideaId
     * @return void
     */
    public void deleteOneIdeaById(Long ideaId);

    /**
     * 增加想法
     *
     * @param idea
     * @return void
     */
    public void addNewIdea(Idea idea);


    /**
     * 删除某一类别
     *
     * @param categoryId
     * @return void
     */
    public void deleteOneCategoryById(Long categoryId);

    /**
     * 删除某一类别下的所有想法
     *
     * @param categoryId
     * @return void
     */
    public void deleteIdeasByCategoryId(Long categoryId);


    /**
     * 增加新类别
     *
     * @param category
     * @return void
     */
    public void addNewCategory(Category category);


    /**
     * 查询一个想法
     *
     * @param ideaId
     * @return Idea
     */
    public Idea selectOneIdeaById(Long ideaId);

    /**
     * 修改是否喜欢某个想法
     *
     * @param idea
     * @return void
     */
    public void updateMarkByIdeaId(Idea idea);

    /**
     * 根据版本id查询projectName
     *
     * @param projectId
     * @return String
     */
    public String selectProjectNameByProjectId(Long projectId);
}
