package com.xmu.nonlinearvisualization.mapper;

import com.xmu.nonlinearvisualization.entity.Version;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 与版本相关的mapper
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public interface VersionMapper {


    /**
     * 删除一个版本
     *
     * @param versionId
     * @return void
     */
    void deleteOneVersionById(Long versionId);

    /**
     * 添加一个版本
     *
     * @param version
     * @return void
     */
    void addNewVersion(Version version);


    /**
     * 得到一个版本
     *
     * @param versionId
     * @return version
     */
    Version getOneVersionById(Long versionId);


    /**
     * 更新order
     *
     * @param version
     * @return
     */
    void updateOrderByVersion(Version version);


    /**
     * 修改版本名称
     *
     * @param version
     * @return
     */
    void modifyVersionName(Version version);

    /**
     * 修改版本信息
     *
     * @param version
     * @return
     */
    void modifyVersionInfo(Version version);

    /**
     * 根据版本id得到version
     *
     * @param projectId
     * @return List<Version>
     */
    List<Version> getAllVersionByProjectId(Long projectId);


    /**
     * 根据项目id和次序得到version
     *
     * @param version
     * @return Version
     */
    Version getOneVersionByProjectIdAndOrder(Version version);

}
