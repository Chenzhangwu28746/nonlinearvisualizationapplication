package com.xmu.nonlinearvisualization.mapper;


import com.xmu.nonlinearvisualization.entity.EdgeDefine;
import com.xmu.nonlinearvisualization.entity.NodeDefine;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与define阶段图形相关的mapper
 *
 * @author ahua
 * @date 2020/1/8
 */
@Repository
public interface SvgDefineMapper {
    /**
     * 得到所有nodes
     *
     * @param projectId
     * @return List<NodeDefine>
     */
    List<NodeDefine> findAllNodesByProjectId(Long projectId);

    /**
     * 得到所有与node相关的edge
     *
     * @param sourceNodeId
     * @return List<EdgeDefine>
     */
    List<EdgeDefine> findAllEdgesByNodeDefineId(Long sourceNodeId);


    /**
     * 更改nodeContent
     *
     * @param nodeDefine
     * @return
     */
    void modifyContentByNodeId(NodeDefine nodeDefine);

    /**
     * 添加结点
     *
     * @param nodeDefine
     * @return Long
     */
    Long applyNewNodeDefine(NodeDefine nodeDefine);

    /**
     * 添加新边
     *
     * @param edgeDefine
     * @return void
     */
    void insertNewEdgeDefine(EdgeDefine edgeDefine);

    /**
     * 找到新边
     *
     * @param nodeId
     * @return NodeDefine
     */
    NodeDefine findOneNodeByNodeId(Long nodeId);


    /**
     * 删除边
     *
     * @param nodeId
     * @return void
     */
    void deleteOneEdgeByTargetNodeId(Long nodeId);

    /**
     * 删除结点
     *
     * @param nodeId
     * @return void
     */
    void deleteOneNodeByNodeId(Long nodeId);
}
