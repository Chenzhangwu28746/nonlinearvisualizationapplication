package com.xmu.nonlinearvisualization.handler;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * 相当于controller的处理器
 */
public class MyHandler extends TextWebSocketHandler {


    String currentData = null;
    /**
     * 接收main方法中传递过来的值
     * @param AndriodData
     * @return
     */
    @PostMapping(value = "/getAndroidValue")
    @ResponseBody  //@ResponseBody的作用：将目标方法的返回值自动转换成json格式，然后返回给前端
    public String httptest(@RequestBody String AndriodData) {
        System.out.println("Controller中获取到的main方法中传递的值：" + AndriodData);
        currentData = AndriodData;
        return AndriodData;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println("看看handleTextMessage里面有没有接收到currentData：" + currentData);
        String payload = message.getPayload();
        Map<String, String> map = JSONObject.parseObject(payload, HashMap.class);
        System.out.println("=====接受到的数据" + map);
        //session.sendMessage(new TextMessage("服务器返回收到的信息," + payload));
        session.sendMessage(new TextMessage(currentData));
    }




}
