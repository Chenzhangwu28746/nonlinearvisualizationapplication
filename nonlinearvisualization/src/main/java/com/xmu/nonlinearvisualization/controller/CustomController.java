package com.xmu.nonlinearvisualization.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping()
public class CustomController {
    @GetMapping("/webSocket")
    public String test1() {
        return "views/webSocket";
    }


    @GetMapping("/test3")
    public String test3() {
        return "views/webSocket2/test3";
    }


    @GetMapping("/threeDScene")
    public String threeDScene() {
        return "views/threeDScene";
    }



    @GetMapping("/DrawingBoard")
    public String DrawingBoard() {
        return "views/DrawingBoard";
    }


    @GetMapping("/testjsMind")
    public String testjsMind(){
        return "views/testjsMind";
    }


}
