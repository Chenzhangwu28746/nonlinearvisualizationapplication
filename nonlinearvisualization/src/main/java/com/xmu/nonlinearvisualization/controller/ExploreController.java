package com.xmu.nonlinearvisualization.controller;


import com.xmu.nonlinearvisualization.entity.MyFile;
import com.xmu.nonlinearvisualization.service.MyFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * explore界面
 *
 * @author ahua
 * @date 2020/1/7
 */
@Controller
public class ExploreController {

    private MyFileService myFileService;


    @Autowired
    ExploreController(MyFileService myFileService) {
        this.myFileService = myFileService;
    }

    @GetMapping(value = "/explore/{account}/{curProjectId}/{curVersionId}")
    public String getExplorePage(@PathVariable("account") Long account,
                                 @PathVariable("curProjectId") Long curProjectId,
                                 @PathVariable("curVersionId") Long curVersionId, Model model) {
        List<MyFile> myFileList = new ArrayList<MyFile>();
        myFileList = myFileService.getAllFilesByCurProjectId(curProjectId);
        model.addAttribute("myFileList", myFileList);
        model.addAttribute("account", account);
        model.addAttribute("curProjectId", curProjectId);
        model.addAttribute("curVersionId", curVersionId);

        return "views/explorePage";
    }


    /**
     * 文件上传具体实现方法;
     *
     * @param file
     * @return
     */
    @PostMapping(value = "/uploadSingleFile")
    @ResponseBody
    public String uploadSingleFile(@RequestParam("file") MultipartFile file, Long curProjectId, Model model) {
        return myFileService.insertNewFile(file, curProjectId);

    }


    /**
     * 删除一个文件;
     *
     * @param myFileId
     * @return
     */
    @PostMapping(value = "/deleteOneFile")
    @ResponseBody
    public String deleteOneFileById(Long myFileId) {
        myFileService.deleteOneFileById(myFileId);
        return "200";
    }


    /**
     * 打开一个docx文件;
     *
     * @param myFileId
     * @return
     */
    @PostMapping(value = "/openOneDocxFile")
    @ResponseBody
    public String openOneDocxFileById(Long myFileId) {

        return myFileService.openOneDocxFileById(myFileId);
    }


    /**
     * 根据文件id找文件名;
     *
     * @param myFileId
     * @return myFileName
     */
    @PostMapping(value = "/findOneFileNameByFileId")
    @ResponseBody
    public String findOneFileNameByFileId(Long myFileId) {
        return myFileService.findOneFileNameByFileId(myFileId);
    }

    /**
     * 根据文件id找文件信息;
     *
     * @param myFileId
     * @return myFileInfo
     */
    @PostMapping(value = "/findOneFileInfoByFileId")
    @ResponseBody
    public String findOneFileInfoByFileId(Long myFileId) {
        return myFileService.findOneFileInfoByFileId(myFileId);
    }


    @PostMapping(value = "/modifyFileInfo")
    @ResponseBody
    public String modifyFileInfo(Long fileId, String newFileInfo) {
        this.myFileService.modifyFileInfo(fileId, newFileInfo);
        return "200";
    }


}