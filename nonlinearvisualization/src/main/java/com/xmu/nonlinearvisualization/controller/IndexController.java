package com.xmu.nonlinearvisualization.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class IndexController {

    @GetMapping("/index")
    public String index(){
        return "views/webSocket2/index";
    }
    //发送指定用户
    @GetMapping("/user")
    public String user(Long id, ModelMap model){
        model.addAttribute("id",id);
        return "views/webSocket2/user";
    }




}
