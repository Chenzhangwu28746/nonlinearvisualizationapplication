package com.xmu.nonlinearvisualization.controller;


import com.xmu.nonlinearvisualization.entity.EdgeDefine;
import com.xmu.nonlinearvisualization.entity.NodeDefine;
import com.xmu.nonlinearvisualization.service.SvgDefineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * define界面
 *
 * @author ahua
 * @date 2020/1/7
 */
@Controller
public class DefineController {

    SvgDefineService svgDefineService;

    @Autowired
    DefineController(SvgDefineService svgDefineService) {
        this.svgDefineService = svgDefineService;
    }

    @GetMapping(value = "/define/{account}/{curProjectId}/{curVersionId}")
    public String getDefinePage(@PathVariable(name = "account") Long account,
                                @PathVariable(name = "curProjectId") Long curProjectId,
                                @PathVariable(name = "curVersionId") Long curVersionId, Model model) {
        model.addAttribute("account", account);
        model.addAttribute("curProjectId", curProjectId);
        model.addAttribute("curVersionId", curVersionId);
        return "views/definePage";
    }

    @PostMapping(value = "/findAllNodesByProjectId")
    @ResponseBody
    public List<NodeDefine> findAllNodesByProjectId(Long curProjectId) {
        List<NodeDefine> nodeDefineList = this.svgDefineService.findAllNodesByProjectId(curProjectId);
        return nodeDefineList;
    }


    @PostMapping(value = "/findAllEdgesByProjectId")
    @ResponseBody
    public List<EdgeDefine> findAllEdgesByProjectId(Long curProjectId) {
        List<EdgeDefine> edgeDefineList = this.svgDefineService.findAllEdgesByProjectId(curProjectId);
        return edgeDefineList;
    }

    @PostMapping(value = "/modifyContentByNodeId")
    @ResponseBody
    public String modifyContentByNodeId(Long nodeDefineId, String newContent) {
        this.svgDefineService.modifyContentByNodeId(nodeDefineId, newContent);
        return "200";
    }

    @PostMapping(value = "/insertNewNodeAndNewEdge")
    @ResponseBody
    public NodeDefine insertNewNodeAndNewEdge(Long curProjectId, Long nodeId) {
        return this.svgDefineService.insertNewNodeAndNewEdge(curProjectId, nodeId);
    }


    @PostMapping(value = "/addSingleNode")
    @ResponseBody
    public NodeDefine addSingleNode(Long curProjectId) {
        return this.svgDefineService.addSingleNode(curProjectId);
    }

    @PostMapping(value = "/deleteOneNodeByNodeId")
    @ResponseBody
    public String deleteOneNodeByNodeId(Long nodeId) {
        this.svgDefineService.deleteOneNodeByNodeId(nodeId);
        return "200";
    }


}