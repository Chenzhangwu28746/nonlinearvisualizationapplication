package com.xmu.nonlinearvisualization.controller;


import com.xmu.nonlinearvisualization.entity.Version;
import com.xmu.nonlinearvisualization.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * test界面
 *
 * @author ahua
 * @date 2020/1/7
 */

@Controller
public class TestController {

    VersionService versionService;

    @Autowired
    TestController(VersionService versionService) {
        this.versionService = versionService;
    }

    @GetMapping(value = "/test/{account}/{curProjectId}/{curVersionId}")
    public String getTestPage(@PathVariable(name = "account") Long account,
                              @PathVariable(name = "curProjectId") Long curProjectId,
                              @PathVariable(name = "curVersionId") Long curVersionId, Model model) {
        List<Version> versionList = versionService.getAllVersionByProjectId(curProjectId);
        model.addAttribute("account", account);
        model.addAttribute("curProjectId", curProjectId);
        model.addAttribute("curVersionId", curVersionId);
        model.addAttribute("versions", versionList);

        return "views/testPage";
    }

    @PostMapping(value = "/deleteOneVersionById")
    @ResponseBody
    public String deleteOneVersionById(Long curProjectId, Long versionId) {
        return this.versionService.deleteOneVersionById(curProjectId, versionId);
    }


    @PostMapping(value = "/addNewVersion")
    @ResponseBody
    public String addNewVersion(Long curProjectId, String versionName) {
        this.versionService.addNewVersion(curProjectId, versionName);
        return "200";
    }


    @PostMapping(value = "/modifyVersionName")
    @ResponseBody
    public String modifyVersionName(Long versionId, String newVersionName) {
        this.versionService.modifyVersionName(versionId, newVersionName);
        return "200";
    }


    @PostMapping(value = "/modifyVersionInfo")
    @ResponseBody
    public String modifyVersionInfo(Long versionId, String newVersionInfo) {
        this.versionService.modifyVersionInfo(versionId, newVersionInfo);
        return "200";
    }


}