package com.xmu.nonlinearvisualization.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xmu.nonlinearvisualization.entity.Category;
import com.xmu.nonlinearvisualization.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ideate界面
 *
 * @author ahua
 * @date 2020/1/7
 */
@Controller
public class IdeateController {

    private CategoryService categoryService;

    @Autowired
    IdeateController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping(value = "/ideate/{account}/{curProjectId}/{curVersionId}")
    public String getIdeatePage(@PathVariable("account") Long account,
                                @PathVariable("curProjectId") Long curProjectId,
                                @PathVariable(name = "curVersionId") Long curVersionId, Model model) {
        model.addAttribute("account", account);
        model.addAttribute("curProjectId", curProjectId);
        model.addAttribute("curVersionId", curVersionId);
        List<Category> categoryList = this.categoryService.selectCategoryListByProjectId(curProjectId);
        String projectName = this.categoryService.selectProjectNameByProjectId(curProjectId);

        System.out.println("getIdeatePage");
        System.out.println(categoryList);

        Object categoryListJson = JSONObject.toJSON(categoryList);
        model.addAttribute("categoryListJson",categoryListJson);
        model.addAttribute("categoryList", categoryList);

        model.addAttribute("projectName", projectName);


        return "views/ideatePage";
    }



    @GetMapping(value = "/getCategoryList")
    @ResponseBody
    public Object getCategoryList(Long curProjectId){
        List<Category> categoryList = this.categoryService.selectCategoryListByProjectId(curProjectId);
        Object categoryListJson = JSONObject.toJSON(categoryList);
        System.out.println("getCategoryList");
        System.out.println(categoryListJson);
        String JsonToString = JSON.toJSONString(categoryListJson);

//        LongToString  LongToStringObject = new LongToString();
//         LongToStringObject.jsonLoopRequest(JsonToString,categoryListJson);
//         System.out.println(categoryListJson);
//        System.out.println(JsonToString);

        //return JsonToString;





        return categoryListJson;
        //return categoryList;
    }



    @PostMapping(value = "/modifyCategoryName")
    @ResponseBody
    public String modifyCategoryNameById(Long categoryId, String categoryName) {
        this.categoryService.modifyCategoryNameById(categoryId, categoryName);
        return "200";
    }

    @PostMapping(value = "/modifyIdeaContent")
    @ResponseBody
    public String modifyIdeaContent(Long ideaId, String ideaContent) {
        this.categoryService.modifyIdeaContent(ideaId, ideaContent);
        return "200";
    }

    @PostMapping(value = "/deleteOneIdeaById")
    @ResponseBody
    public String deleteOneIdeaById(Long ideaId) {
        this.categoryService.deleteOneIdeaById(ideaId);
        return "200";
    }


    @PostMapping(value = "/addNewIdea")
    @ResponseBody
    public String addNewIdea(Long categoryId, String ideaContent) {
        return this.categoryService.addNewIdea(categoryId, ideaContent);
    }


    @PostMapping(value = "/deleteOneCategoryById")
    @ResponseBody
    public String deleteOneCategoryById(Long categoryId) {
        this.categoryService.deleteOneCategoryById(categoryId);
        return "200";
    }


    @PostMapping(value = "/addNewCategory")
    @ResponseBody
    public String addNewCategory(Long projectId, String categoryName) {
        this.categoryService.addNewCategory(projectId, categoryName);
        return "200";
    }


    @PostMapping(value = "/changeIdeaMarkById")
    @ResponseBody
    public String changeIdeaMarkById(Long ideaId) {
        this.categoryService.changeIdeaMarkById(ideaId);
        return "200";
    }


}