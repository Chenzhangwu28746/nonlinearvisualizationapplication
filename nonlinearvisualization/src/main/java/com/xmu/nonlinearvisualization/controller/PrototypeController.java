package com.xmu.nonlinearvisualization.controller;

import com.xmu.nonlinearvisualization.entity.Figure;
import com.xmu.nonlinearvisualization.entity.Operation;
import com.xmu.nonlinearvisualization.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * prototype界面
 *
 * @author ahua
 * @date 2020/1/7
 */
@Controller
public class PrototypeController {

    private PrototypeService prototypeService;
    private VersionService versionService;
    private FigureService figureService;
    private OperationService operationService;
    private ProjectService projectService;


    @Autowired
    PrototypeController(PrototypeService prototypeService, VersionService versionService,
                        FigureService figureService, OperationService operationService,
                        ProjectService projectService) {
        this.prototypeService = prototypeService;
        this.versionService = versionService;
        this.figureService = figureService;
        this.operationService = operationService;
        this.projectService = projectService;
    }

    @RequestMapping(value = "/enter/{account}/{projectId}")
    public String enterOneProject(@PathVariable(name = "account") Long account,
                                  @PathVariable(name = "projectId") Long projectId, Model model) {
        Long theNewestVersion = projectService.getTheNewestVersionId(projectId);
        List<Figure> figureList = figureService.getAllFigureByVersionId(theNewestVersion);
        List<Operation> operationList = operationService.getAllOperationByVersionId(theNewestVersion);
        model.addAttribute("operationList", operationList);
        model.addAttribute("theNewestVersionId", theNewestVersion);
        model.addAttribute("account", account);
        model.addAttribute("curProjectId", projectId);
        model.addAttribute("curVersionId", theNewestVersion);
        model.addAttribute("figureList", figureList);
        return "views/prototypePage";
    }

    @GetMapping(value = "/prototype/{account}/{curProjectId}/{curVersionId}")
    public String getPrototypePage(@PathVariable(name = "account") Long account,
                                   @PathVariable(name = "curProjectId") Long curProjectId,
                                   @PathVariable(name = "curVersionId") Long curVersionId, Model model) {
        Long theNewestVersion = this.projectService.getTheNewestVersionId(curProjectId);
        model.addAttribute("theNewestVersionId", theNewestVersion);
        model.addAttribute("curVersionId", curVersionId);
        List<Figure> figureList = figureService.getAllFigureByVersionId(curVersionId);
        model.addAttribute("figureList", figureList);
        List<Operation> operationList = this.operationService.getAllOperationByVersionId(curVersionId);
        model.addAttribute("operationList", operationList);
        return "views/prototypePage";
    }

    /**
     * 文件上传具体实现方法;
     *
     * @param file
     * @return
     */
    @PostMapping(value = "/uploadFlowFile")
    @ResponseBody
    public String uploadOperationFile(@RequestParam("file") MultipartFile file, Long curVersionId, Model model) {
        return operationService.updateAllOperationByVersionId(file, Long.valueOf(curVersionId));

    }


}