package com.xmu.nonlinearvisualization.controller;


import com.xmu.nonlinearvisualization.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 选择项目界面
 *
 * @author ahua
 * @date 2020/4/18
 */
@Controller
public class ProjectController {
    private final ProjectService projectService;

    @Autowired
    ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(value = "/projectList/{account}")
    public String projectList(@PathVariable(name = "account") Long account, Model model) {
        List projectList = this.projectService.selectAllProjectByAccount(account);
        model.addAttribute("account", account);
        model.addAttribute("projectList", projectList);
        return "views/projectList";
    }

    @PostMapping(value = "/getTheNewestVersionId")
    @ResponseBody
    public Long getTheNewestVersionId(Long curProjectId) {
        return this.projectService.getTheNewestVersionId(curProjectId);
    }


    @PostMapping(value = "/addNewProject")
    @ResponseBody
    public String addNewProject(Long account, String projectName) {
        this.projectService.addNewProject(account, projectName);
        return "200";
    }


    @PostMapping(value = "/modifyProjectName")
    @ResponseBody
    public String modifyProjectName(Long projectId, String projectName) {
        this.projectService.modifyProjectName(projectId, projectName);
        return "200";
    }

    @PostMapping(value = "/modifyProjectInfo")
    @ResponseBody
    public String modifyProjectInfo(Long projectId, String projectInfo) {
        this.projectService.modifyProjectInfo(projectId, projectInfo);
        return "200";
    }
}
