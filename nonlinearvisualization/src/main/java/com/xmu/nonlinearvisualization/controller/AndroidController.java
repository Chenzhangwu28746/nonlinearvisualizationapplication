package com.xmu.nonlinearvisualization.controller;


import com.xmu.nonlinearvisualization.entity.Greeting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@EnableScheduling
public class AndroidController {



    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SimpMessagingTemplate messagingTemplate;


    String currentData = "Andriod端暂无数据传输！";
    String PreviousData = currentData;

    /**
     * 接收main方法中传递过来的值
     * @param AndriodData
     * @return
     */

    //@PostMapping(value = "/server/httptest")
    @PostMapping(value = "/getAndroidValue")
    @ResponseBody  //@ResponseBody的作用：将目标方法的返回值自动转换成json格式，然后返回给前端
    public String httptest(@RequestBody String AndriodData) {
        System.out.println("AndroidController中获取到的main方法中传递的值：" + AndriodData);
        //model.addAttribute("AndriodData", AndriodData);
        //return "views/ideatePage";
        PreviousData = currentData;
        if(AndriodData != null){
            currentData = AndriodData;
        }


        return AndriodData;
    }


    /**
     * 前端主动获取main方法传递过来的值
     * @return
     */
    @PostMapping(value = "/htmlGetAndroid")
    @ResponseBody
    public String htmlGetAndroid() {
        return currentData;
    }


    /**
     * 后台定时向前台推送信息
     */

    @Scheduled(fixedRate = 5000)
    public void send(){
        if (PreviousData != currentData){
            messagingTemplate.convertAndSend("/topic/greetings", new Greeting(currentData));
            PreviousData = currentData;
        }else{
            //暂时啥也不干
        }

    }




}
