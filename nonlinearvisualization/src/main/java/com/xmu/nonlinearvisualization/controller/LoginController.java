package com.xmu.nonlinearvisualization.controller;

import com.xmu.nonlinearvisualization.entity.Designer;
import com.xmu.nonlinearvisualization.service.DesignerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * login界面
 *
 * @author ahua
 * @date 2020/1/7
 */
@Controller
public class LoginController {

    private final DesignerService designerService;

    @Autowired
    LoginController(DesignerService designerService) {
        this.designerService = designerService;
    }

    @GetMapping(value = "/")
    public String index(Model model) {
        return "views/login";
    }


    @PostMapping(value = "/login")
    @ResponseBody
    public String login(Long account, String password) {
        Designer designer = new Designer();
        designer.setAccount(account);
        designer.setDesignerPassword(password);
        return designerService.hasTheDesigner(designer);
    }


}
