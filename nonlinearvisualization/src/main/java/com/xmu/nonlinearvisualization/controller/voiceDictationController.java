package com.xmu.nonlinearvisualization.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * prototype界面
 *
 * @author zwchen
 * @date 2021/3/11
 */

@Controller
public class voiceDictationController {

    @RequestMapping(value = "/voiceDictation")
    public String enterVoiceDictation() {
        return "views/voiceDictation";
    }
}


