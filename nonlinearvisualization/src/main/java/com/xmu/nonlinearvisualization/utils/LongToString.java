package com.xmu.nonlinearvisualization.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Iterator;
import java.util.Map;

public class LongToString {

    public void replaceRequest( String requestStr ,  String str){
        if(requestStr.charAt(requestStr.indexOf(str)-1)!='\"'){
            requestStr=requestStr.replaceFirst(str,"\""+str+"\"");
        }
    }


    //利用 fastjson 遍历jsonObject
    public  void jsonLoopRequest(String requestStr, Object object) {

        if(object instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) object;
            Iterator iter = jsonObject.entrySet().iterator();

            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object o = entry.getValue();
                if(o instanceof Long) {
                    replaceRequest(requestStr , entry.getValue().toString());
                    //	System.out.println("key:" + entry.getKey() + "，value:" + entry.getValue());
                } else  if(o instanceof JSONArray) {
                    jsonLoopRequest(requestStr, o);
                } else  if(o instanceof  JSONObject) {
                    jsonLoopRequest(requestStr, o);
                }
            }
        }
        if(object instanceof JSONArray) {
            JSONArray jsonArray = (JSONArray) object;
            for(int i = 0; i < jsonArray.size(); i ++) {
                jsonLoopRequest(requestStr , jsonArray.get(i));
            }
        }
    }


}
