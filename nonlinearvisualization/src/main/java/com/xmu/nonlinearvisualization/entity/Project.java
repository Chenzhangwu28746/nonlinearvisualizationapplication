package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/4/18
 */
@Data
public class Project {
    Long id;
    Long projectId;
    Long designerAccount;
    String projectName;
    String projectInfo;
    Long newestVersionId;
    Long newestVersionOrder;
}
