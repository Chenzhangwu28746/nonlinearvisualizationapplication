package com.xmu.nonlinearvisualization.entity;


import lombok.Data;

import java.util.List;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/14
 */
@Data
public class Category {
    Long id;
    Long categoryId;
    String categoryName;
    Long projectId;
    List<Idea> ideaList;
}
