package com.xmu.nonlinearvisualization.entity;

import lombok.Data;


/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/7
 */
@Data
public class MyFile {
    Long id;
    Long myFileId;
    Long projectId;
    String myFileName;
    String myFileInfo;
    String myFileAddress;
}
