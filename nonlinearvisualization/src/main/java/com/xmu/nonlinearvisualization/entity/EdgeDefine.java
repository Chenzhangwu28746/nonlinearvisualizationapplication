package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/8
 */
@Data
public class EdgeDefine {
    Long id;
    Long edgeId;
    Long sourceNodeId;
    Long targetNodeId;
    String edgeContent;
}
