package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/28
 */
@Data
public class Figure {
    Long id;
    Long figureId;
    Long versionId;
    Long figureOrder;
    String figureName;
    String figureInfo;
    String figureAddress;

}
