package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

import java.util.Comparator;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/7
 */
@Data
public class Version {
    Long id;
    Long versionId;
    Long projectId;
    Long versionOrder;
    String versionName;
    String versionInfo;


}
