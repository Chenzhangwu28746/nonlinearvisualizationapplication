package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/4/18
 */
@Data
public class Designer {
    Long id;
    Long account;
    String designerPassword;
}
