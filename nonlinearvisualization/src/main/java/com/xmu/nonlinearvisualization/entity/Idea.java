package com.xmu.nonlinearvisualization.entity;


import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/14
 */
@Data
public class Idea {
    Long id;
    Long ideaId;
    Long categoryId;
    String ideaContent;
    Boolean marked;
}
