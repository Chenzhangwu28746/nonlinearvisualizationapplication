package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/28
 */
@Data
public class Operation {
    Long id;
    Long operationId;
    Long versionId;
    String operationOrder;
    String firstGrammar;
    String secondGrammar;
    String thirdGrammar;
    String fourthGrammar;
}
