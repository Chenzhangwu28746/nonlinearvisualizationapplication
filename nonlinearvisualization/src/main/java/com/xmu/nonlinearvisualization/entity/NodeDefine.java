package com.xmu.nonlinearvisualization.entity;


import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/8
 */
@Data
public class NodeDefine {
    Long id;
    Long nodeId;
    Long projectId;
    String nodeContent;
}
