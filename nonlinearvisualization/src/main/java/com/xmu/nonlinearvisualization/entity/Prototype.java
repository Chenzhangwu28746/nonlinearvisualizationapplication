package com.xmu.nonlinearvisualization.entity;

import lombok.Data;

/**
 * POJO
 *
 * @author ahua
 * @date 2020/1/7
 */
@Data
public class Prototype {
    private Long id;
    private Long prototypeId;
    private String name;
}
