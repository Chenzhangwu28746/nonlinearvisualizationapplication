package com.xmu.nonlinearvisualization.dao;

import com.xmu.nonlinearvisualization.entity.EdgeDefine;
import com.xmu.nonlinearvisualization.entity.NodeDefine;
import com.xmu.nonlinearvisualization.mapper.SvgDefineMapper;
import com.xmu.nonlinearvisualization.service.SvgDefineService;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与define图片相关的dao
 *
 * @author ahua
 * @date 2020/1/8
 */
@Repository
public class SvgDefineDao {
    private final SvgDefineMapper svgDefineMapper;

    SvgDefineDao(SvgDefineMapper svgDefineMapper) {
        this.svgDefineMapper = svgDefineMapper;
    }

    public List<NodeDefine> findAllNodesByProjectId(Long projectId) {
        return this.svgDefineMapper.findAllNodesByProjectId(projectId);
    }


    public List<EdgeDefine> findAllEdgesByNodeDefineId(Long nodeDefineId) {
        return this.svgDefineMapper.findAllEdgesByNodeDefineId(nodeDefineId);
    }

    public void modifyContentByNodeId(NodeDefine nodeDefine) {
        this.svgDefineMapper.modifyContentByNodeId(nodeDefine);
    }

    public Long applyNewNodeDefine(NodeDefine nodeDefine) {
        return this.svgDefineMapper.applyNewNodeDefine(nodeDefine);
    }

    public void insertNewEdgeDefine(EdgeDefine edgeDefine) {
        this.svgDefineMapper.insertNewEdgeDefine(edgeDefine);
    }

    public void deleteOneNodeByNodeId(Long nodeId) {
        this.svgDefineMapper.deleteOneNodeByNodeId(nodeId);
    }

    public void deleteOneEdgeByTargetNodeId(Long nodeId) {
        this.svgDefineMapper.deleteOneEdgeByTargetNodeId(nodeId);
    }


}
