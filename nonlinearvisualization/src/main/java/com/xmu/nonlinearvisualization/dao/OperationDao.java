package com.xmu.nonlinearvisualization.dao;


import com.xmu.nonlinearvisualization.entity.Operation;
import com.xmu.nonlinearvisualization.mapper.OperationMapper;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;

/**
 * 与操作相关的dao
 *
 * @author ahua
 * @date 2020/1/28
 */
@Repository
public class OperationDao {
    private final OperationMapper operationMapper;

    OperationDao(OperationMapper operationMapper) {
        this.operationMapper = operationMapper;
    }

    public List<Operation> getAllOperationByVersionId(Long versionId) {
        List<Operation> operationList = this.operationMapper.getAllOperationByVersionId(versionId);
        operationList.sort(new Comparator<Operation>() {
            @Override
            public int compare(Operation o1, Operation o2) {
                Long result = Long.valueOf(o1.getOperationOrder()) - Long.valueOf(o2.getOperationOrder());
                return result.intValue();
            }
        });
        return operationList;
    }
}
