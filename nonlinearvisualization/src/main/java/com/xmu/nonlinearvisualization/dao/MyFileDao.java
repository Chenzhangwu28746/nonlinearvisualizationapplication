package com.xmu.nonlinearvisualization.dao;


import com.xmu.nonlinearvisualization.entity.MyFile;
import com.xmu.nonlinearvisualization.mapper.MyFileMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 与文件相关的dao
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public class MyFileDao {
    private final MyFileMapper myFileMapper;

    MyFileDao(MyFileMapper myFileMapper) {
        this.myFileMapper = myFileMapper;
    }

    public void insertNewFile(MyFile myFile) {
        this.myFileMapper.insertNewFile(myFile);
    }


    public int countSameFile(String fileName) {
        return this.myFileMapper.countSameFile(fileName);
    }


    public List<MyFile> getAllFilesByCurProjectId(Long curProjectId) {
        return this.myFileMapper.getAllFilesByCurProjectId(curProjectId);
    }

    public void deleteOneFileById(Long myFileId) {
        System.out.println(myFileId);
        this.myFileMapper.deleteOneFileById(myFileId);
    }

    public String findFileAddressByMyFileId(Long myFileId) {
        return this.myFileMapper.findFileAddressByMyFileId(myFileId);
    }

    public String findOneFileNameByFileId(Long myFileId) {
        return this.myFileMapper.findOneFileNameByFileId(myFileId);
    }

    public String findOneFileInfoByFileId(Long myFileId) {
        return this.myFileMapper.findOneFileInfoByFileId(myFileId);
    }

    public void modifyFileInfo(MyFile myFile) {
        this.myFileMapper.modifyFileInfo(myFile);
    }
}
