package com.xmu.nonlinearvisualization.dao;

import com.xmu.nonlinearvisualization.entity.Figure;
import com.xmu.nonlinearvisualization.entity.Version;
import com.xmu.nonlinearvisualization.mapper.FigureMapper;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;

/**
 * 与轮播图片相关的dao
 *
 * @author ahua
 * @date 2020/1/28
 */
@Repository
public class FigureDao {
    private final FigureMapper figureMapper;

    FigureDao(FigureMapper figureMapper) {
        this.figureMapper = figureMapper;
    }


    public List<Figure> getAllFigureByVersionId(Long versionId) {
        List<Figure> figureList = this.figureMapper.getAllFigureByVersionId(versionId);
        figureList.sort(new Comparator<Figure>() {
            @Override
            public int compare(Figure f1, Figure f2) {
                Long result = f1.getFigureOrder() - f2.getFigureOrder();
                return result.intValue();
            }
        });
        return figureList;
    }
}
