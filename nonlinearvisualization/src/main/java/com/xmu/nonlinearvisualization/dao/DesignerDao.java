package com.xmu.nonlinearvisualization.dao;


import com.xmu.nonlinearvisualization.entity.Designer;
import com.xmu.nonlinearvisualization.mapper.DesignerMapper;
import org.springframework.stereotype.Repository;

/**
 * 与设计者类别相关的dao
 *
 * @author ahua
 * @date 2020/4/18
 */
@Repository
public class DesignerDao {
    private final DesignerMapper designerMapper;

    DesignerDao(DesignerMapper designerMapper) {
        this.designerMapper = designerMapper;
    }


    public String hasTheDesigner(Designer designer) {
        if (designerMapper.hasTheDesigner(designer) >= 1) {
            return "YES";
        }
        return "NO";
    }


}
