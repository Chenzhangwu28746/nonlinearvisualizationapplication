package com.xmu.nonlinearvisualization.dao;

import com.xmu.nonlinearvisualization.entity.Version;
import com.xmu.nonlinearvisualization.mapper.VersionMapper;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;


/**
 * 与版本相关的dao
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public class VersionDao {
    private final VersionMapper versionMapper;

    VersionDao(VersionMapper versionMapper) {
        this.versionMapper = versionMapper;
    }

    public List<Version> getAllVersionByProjectId(Long projectId) {
        List allVersions = versionMapper.getAllVersionByProjectId(projectId);
        allVersions.sort(new Comparator<Version>() {
            @Override
            public int compare(Version v1, Version v2) {
                Long result = v1.getVersionOrder() - v2.getVersionOrder();
                return result.intValue();
            }
        });
        return allVersions;
    }


    public void addNewVersion(Version version) {
        this.versionMapper.addNewVersion(version);
    }


    public void deleteOneVersionById(Long versionId) {
        versionMapper.deleteOneVersionById(versionId);
    }

    public Version getOneVersionById(Long versionId) {
        return this.versionMapper.getOneVersionById(versionId);
    }


    public void updateOrderByVersion(Version version) {
        this.versionMapper.updateOrderByVersion(version);
    }

    public void modifyVersionName(Version version) {
        this.versionMapper.modifyVersionName(version);
    }

    public void modifyVersionInfo(Version version) {
        this.versionMapper.modifyVersionInfo(version);
    }

    public Version getOneVersionByProjectIdAndOrder(Long projectId, Long order) {
        Version version = new Version();
        version.setProjectId(projectId);
        version.setVersionOrder(order);
        return this.versionMapper.getOneVersionByProjectIdAndOrder(version);
    }
}
