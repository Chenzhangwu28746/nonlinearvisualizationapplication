package com.xmu.nonlinearvisualization.dao;


import com.xmu.nonlinearvisualization.entity.Project;
import com.xmu.nonlinearvisualization.entity.Version;
import com.xmu.nonlinearvisualization.mapper.ProjectMapper;
import com.xmu.nonlinearvisualization.mapper.VersionMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与项目相关的dao
 *
 * @author ahua
 * @date 2020/4/18
 */
@Repository
public class ProjectDao {
    private final ProjectMapper projectMapper;

    ProjectDao(ProjectMapper projectMapper) {
        this.projectMapper = projectMapper;
    }

    public List<Project> selectAllProjectByAccount(Long account) {
        return this.projectMapper.selectAllProjectByAccount(account);
    }

    public Long getTheNewestVersionId(Long projectId) {
        return this.projectMapper.getTheNewestVersionId(projectId);
    }

    public Long getTheNewestVersionOrder(Long projectId) {
        return this.projectMapper.getTheNewestVersionOrder(projectId);
    }

    public Long getVersionNumByProjectId(Long projectId) {
        return this.projectMapper.getVersionNumByProjectId(projectId);
    }

    public void updateTheNewestVersion(Long projectId, Long versionId, Long versionOrder) {
        Project project = new Project();
        project.setProjectId(projectId);
        project.setNewestVersionId(versionId);
        project.setNewestVersionOrder(versionOrder);
        this.projectMapper.updateTheNewestVersion(project);
    }

    public void addNewProject(Project project) {
        this.projectMapper.addNewProject(project);
    }

    public void modifyProjectName(Project project) {
        this.projectMapper.modifyProjectName(project);
    }

    public void modifyProjectInfo(Project project) {
        this.projectMapper.modifyProjectInfo(project);
    }
}
