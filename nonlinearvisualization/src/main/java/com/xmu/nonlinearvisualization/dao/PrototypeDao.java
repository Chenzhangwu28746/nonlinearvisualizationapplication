package com.xmu.nonlinearvisualization.dao;


import com.xmu.nonlinearvisualization.entity.Prototype;
import com.xmu.nonlinearvisualization.mapper.PrototypeMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与prototype相关的dao
 *
 * @author ahua
 * @date 2020/1/7
 */
@Repository
public class PrototypeDao {
    private final PrototypeMapper prototypeMapper;

    PrototypeDao(PrototypeMapper prototypeMapper) {
        this.prototypeMapper = prototypeMapper;
    }

    public List<Prototype> selectPrototypeById(Long prototypeId) {
        return prototypeMapper.selectPrototypeById(prototypeId);
    }


}
