package com.xmu.nonlinearvisualization.dao;

import com.xmu.nonlinearvisualization.entity.Category;
import com.xmu.nonlinearvisualization.entity.Idea;
import com.xmu.nonlinearvisualization.mapper.CategoryMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 与想法类别相关的dao
 *
 * @author ahua
 * @date 2020/1/14
 */
@Repository
public class CategoryDao {
    private final CategoryMapper categoryMapper;

    CategoryDao(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    public List<Category> selectCategoryListByProjectId(Long projectId) {
        return this.categoryMapper.selectCategoryListByProjectId(projectId);
    }

    public void modifyCategoryName(Category category) {
        this.categoryMapper.modifyCategoryName(category);
    }

    public void modifyIdeaContent(Idea idea) {
        this.categoryMapper.modifyIdeaContent(idea);
    }

    public void deleteOneIdeaById(Long ideaId) {
        this.categoryMapper.deleteOneIdeaById(ideaId);
    }

    public void addNewIdea(Idea idea) {
        this.categoryMapper.addNewIdea(idea);
    }


    public void deleteIdeasByCategoryId(Long categoryId) {
        this.categoryMapper.deleteIdeasByCategoryId(categoryId);
    }

    public void deleteOneCategoryById(Long categoryId) {
        this.categoryMapper.deleteOneCategoryById(categoryId);
    }

    public void addNewCategory(Category category) {
        this.categoryMapper.addNewCategory(category);
    }


    public void changeIdeaMarkById(Long ideaId) {
        Idea oldIdea = this.categoryMapper.selectOneIdeaById(ideaId);
        if (oldIdea.getMarked() == true) {
            oldIdea.setMarked(false);
        } else {
            oldIdea.setMarked(true);
        }
        this.categoryMapper.updateMarkByIdeaId(oldIdea);

    }

    public String selectProjectNameByProjectId(Long projectId) {
        return this.categoryMapper.selectProjectNameByProjectId(projectId);
    }
}
