package com.xmu.nonlinearvisualization.service;

//import com.sun.org.apache.regexp.internal.RE;

import com.xmu.nonlinearvisualization.dao.SvgDefineDao;
import com.xmu.nonlinearvisualization.entity.EdgeDefine;
import com.xmu.nonlinearvisualization.entity.NodeDefine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * SvgDefineService
 *
 * @author ahua
 * @date 2020/1/8
 */
@Service
public class SvgDefineService {
    private SvgDefineDao svgDefineDao;

    @Autowired
    SvgDefineService(SvgDefineDao svgDefineDao) {
        this.svgDefineDao = svgDefineDao;
    }

    public List<NodeDefine> findAllNodesByProjectId(Long projectId) {
        return this.svgDefineDao.findAllNodesByProjectId(projectId);
    }

    public List<EdgeDefine> findAllEdgesByProjectId(Long projectId) {
        List<EdgeDefine> edgeDefineList = new ArrayList<EdgeDefine>();
        List<NodeDefine> nodeDefineList = this.svgDefineDao.findAllNodesByProjectId(projectId);
        for (NodeDefine nodeDefine : nodeDefineList) {
            List<EdgeDefine> edgeDefines = this.svgDefineDao.findAllEdgesByNodeDefineId(nodeDefine.getNodeId());
            edgeDefineList.addAll(edgeDefines);
        }
        return edgeDefineList;
    }

    public void modifyContentByNodeId(Long nodeDefineId, String newContent) {
        NodeDefine nodeDefine = new NodeDefine();
        nodeDefine.setNodeId(nodeDefineId);
        nodeDefine.setNodeContent(newContent);
        this.svgDefineDao.modifyContentByNodeId(nodeDefine);
    }


    public NodeDefine insertNewNodeAndNewEdge(Long projectId, Long nodeId) {
        NodeDefine newNodeDefine = new NodeDefine();
        newNodeDefine.setProjectId(projectId);
        Calendar c = Calendar.getInstance();
        String newNodeId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND);

        newNodeDefine.setNodeId(Long.valueOf(newNodeId));
        newNodeDefine.setNodeContent("某个想法");
        this.svgDefineDao.applyNewNodeDefine(newNodeDefine);

        EdgeDefine newEdgeDefine = new EdgeDefine();
        newEdgeDefine.setSourceNodeId(nodeId);
        newEdgeDefine.setTargetNodeId(newNodeDefine.getNodeId());
        this.svgDefineDao.insertNewEdgeDefine(newEdgeDefine);

        return newNodeDefine;
    }

    public NodeDefine addSingleNode(Long projectId) {
        NodeDefine newNodeDefine = new NodeDefine();
        newNodeDefine.setProjectId(projectId);
        Calendar c = Calendar.getInstance();
        String newNodeId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND);

        newNodeDefine.setNodeId(Long.valueOf(newNodeId));
        newNodeDefine.setNodeContent("某个想法");
        this.svgDefineDao.applyNewNodeDefine(newNodeDefine);

        return newNodeDefine;
    }


    public void deleteOneNodeByNodeId(Long nodeId) {
        this.svgDefineDao.deleteOneEdgeByTargetNodeId(nodeId);
        this.svgDefineDao.deleteOneNodeByNodeId(nodeId);
    }


}
