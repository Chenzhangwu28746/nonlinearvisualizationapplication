package com.xmu.nonlinearvisualization.service;


import com.xmu.nonlinearvisualization.dao.CategoryDao;
import com.xmu.nonlinearvisualization.entity.Category;
import com.xmu.nonlinearvisualization.entity.Idea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * file service
 *
 * @author ahua
 * @date 2020/1/14
 */
@Service
public class CategoryService {
    private CategoryDao categoryDao;


    @Autowired
    CategoryService(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }


    public List<Category> selectCategoryListByProjectId(Long projectId) {
        List<Category> categoryList = this.categoryDao.selectCategoryListByProjectId(projectId);
        return categoryList;
    }

    public String selectProjectNameByProjectId(Long projectId){
        String projectName = this.categoryDao.selectProjectNameByProjectId(projectId);
        return projectName;
    }

    public void modifyCategoryNameById(Long categoryId, String categoryName) {
        Category category = new Category();
        category.setCategoryId(categoryId);
        category.setCategoryName(categoryName);
        this.categoryDao.modifyCategoryName(category);
    }

    public void modifyIdeaContent(Long ideaId, String ideaContent) {
        Idea idea = new Idea();
        idea.setIdeaId(ideaId);
        idea.setIdeaContent(ideaContent);
        this.categoryDao.modifyIdeaContent(idea);
    }


    public void deleteOneIdeaById(Long ideaId) {
        this.categoryDao.deleteOneIdeaById(ideaId);
    }

    public String addNewIdea(Long categoryId, String ideaContent) {
        Idea idea = new Idea();
        Calendar c = Calendar.getInstance();
        String newIdeaId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND);

        idea.setIdeaId(Long.valueOf(newIdeaId));
        idea.setCategoryId(categoryId);
        /**
         * czw
         */
//        ideaContent =   "'"+ ideaContent + "'";
        idea.setIdeaContent(ideaContent);

        this.categoryDao.addNewIdea(idea);
        return newIdeaId;
    }


    public void deleteOneCategoryById(Long categoryId) {
        this.categoryDao.deleteIdeasByCategoryId(categoryId);
        this.categoryDao.deleteOneCategoryById(categoryId);
    }

    public void addNewCategory(Long projectId, String categoryName) {
        Category category = new Category();
        category.setProjectId(projectId);
        category.setCategoryName(categoryName);
        this.categoryDao.addNewCategory(category);
    }

    public void changeIdeaMarkById(Long ideaId) {
        this.categoryDao.changeIdeaMarkById(ideaId);
    }


}
