package com.xmu.nonlinearvisualization.service;

import com.xmu.nonlinearvisualization.dao.MyFileDao;
import com.xmu.nonlinearvisualization.entity.MyFile;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * file service
 *
 * @author ahua
 * @date 2020/1/7
 */
@Service
public class MyFileService {
    private MyFileDao myFileDao;

    @Autowired
    MyFileService(MyFileDao myFileDao) {
        this.myFileDao = myFileDao;
    }

    public String insertNewFile(MultipartFile file, Long projectId) {
        String fileName = file.getOriginalFilename();
        String destFileName = "C:\\Users\\HP\\Desktop\\mster\\nonlinearvisualization\\src\\main\\resources\\static\\files\\" + projectId + "\\" + fileName;
        File destFile = new File(destFileName);
        if (myFileDao.countSameFile(destFileName) > 0) {
            return "400";
        }
        try {
            destFile.getParentFile().mkdirs();
            file.transferTo(destFile);
            MyFile myFile = new MyFile();
            myFile.setMyFileName(fileName);
            myFile.setMyFileAddress(destFileName);
            myFile.setProjectId(projectId);
            myFile.setMyFileInfo(fileName);
            myFileDao.insertNewFile(myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "400";
        } catch (IOException e) {
            e.printStackTrace();
            return "400";
        }
        return "200";
    }


    public List<MyFile> getAllFilesByCurProjectId(Long curProjectId) {
        return myFileDao.getAllFilesByCurProjectId(curProjectId);
    }

    public void deleteOneFileById(Long myFileId) {
        myFileDao.deleteOneFileById(myFileId);
    }

    public String openOneDocxFileById(Long myFileId) {
        String myFileAddress = this.myFileDao.findFileAddressByMyFileId(myFileId);
        File docxFile = new File(myFileAddress);
        FileInputStream fis = null;
        String docContent = null;
        String fileName = docxFile.getName();
        try {
            fis = new FileInputStream(docxFile);
            if (fileName.toLowerCase().endsWith("doc")) {
                HWPFDocument doc = new HWPFDocument(fis);
                docContent = doc.getDocumentText();
            } else if (fileName.toLowerCase().endsWith("docx")) {
                XWPFDocument xdoc = new XWPFDocument(fis);
                XWPFWordExtractor extractor = new XWPFWordExtractor(xdoc);
                docContent = extractor.getText();
            }
        } catch (Exception e) {
        }
        return docContent;
    }


    public String findOneFileNameByFileId(Long myFileId) {
        return this.myFileDao.findOneFileNameByFileId(myFileId);
    }

    public String findOneFileInfoByFileId(Long myFileId) {
        return this.myFileDao.findOneFileInfoByFileId(myFileId);
    }

    public void modifyFileInfo(Long fileId, String newFileInfo) {
        MyFile myFile = new MyFile();
        myFile.setMyFileId(fileId);
        myFile.setMyFileInfo(newFileInfo);
        this.myFileDao.modifyFileInfo(myFile);
    }

}
