package com.xmu.nonlinearvisualization.service;

import com.xmu.nonlinearvisualization.dao.DesignerDao;
import com.xmu.nonlinearvisualization.entity.Designer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * designer service
 *
 * @author ahua
 * @date 2020/4/18
 */
@Service
public class DesignerService {
    private DesignerDao designerDao;

    //nonlinearvisualization@localhost
    @Autowired
    DesignerService(DesignerDao designerDao) {
        this.designerDao = designerDao;
    }

    public String hasTheDesigner(Designer designer) {
        return designerDao.hasTheDesigner(designer);
    }

}
