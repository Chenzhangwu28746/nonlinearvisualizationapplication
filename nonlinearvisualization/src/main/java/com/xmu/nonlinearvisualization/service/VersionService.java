package com.xmu.nonlinearvisualization.service;

import com.xmu.nonlinearvisualization.dao.ProjectDao;
import com.xmu.nonlinearvisualization.dao.PrototypeDao;
import com.xmu.nonlinearvisualization.dao.VersionDao;
import com.xmu.nonlinearvisualization.entity.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

/**
 * version service
 *
 * @author ahua
 * @date 2020/1/7
 */
@Service
public class VersionService {
    private VersionDao versionDao;
    private ProjectDao projectDao;

    @Autowired
    VersionService(VersionDao versionDao, ProjectDao projectDao) {
        this.versionDao = versionDao;
        this.projectDao = projectDao;
    }


    public List<Version> getAllVersionByProjectId(Long projectId) {
        return this.versionDao.getAllVersionByProjectId(projectId);
    }


    public void addNewVersion(Long projectId, String versionName) {
        Version newVersion = new Version();
        newVersion.setVersionName(versionName);
        newVersion.setProjectId(projectId);
        Calendar c = Calendar.getInstance();
        String versionId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND);

        newVersion.setVersionId(Long.valueOf(versionId));
        Long curNewestVersionOrder = this.projectDao.getTheNewestVersionOrder(projectId);
        newVersion.setVersionOrder(curNewestVersionOrder + 1);
        newVersion.setVersionInfo(versionName);
        this.versionDao.addNewVersion(newVersion);
        this.projectDao.updateTheNewestVersion(projectId, newVersion.getVersionId(), newVersion.getVersionOrder());

    }


    public String deleteOneVersionById(Long curProjectId, Long versionId) {
        Long lastNewestVersionId = this.projectDao.getTheNewestVersionId(curProjectId);
        Version lastNewestVersion = this.versionDao.getOneVersionById(lastNewestVersionId);
        Version curVersion = this.versionDao.getOneVersionById(versionId);
        Long versionNum = this.projectDao.getVersionNumByProjectId(curProjectId);
        if (lastNewestVersionId.equals(versionId)) {
            Version nextNewestVersion = this.versionDao.getOneVersionByProjectIdAndOrder(curProjectId, versionNum - 1);
            this.projectDao.updateTheNewestVersion(curProjectId, nextNewestVersion.getVersionId(), nextNewestVersion.getVersionOrder());
            this.versionDao.deleteOneVersionById(lastNewestVersionId);
            //此处应该添加删除与该版本相关信息的代码
        } else {
            this.versionDao.deleteOneVersionById(curVersion.getVersionId());
            Long lastNewestVersionOrder = lastNewestVersion.getVersionOrder();
            Long curVersionOrder = curVersion.getVersionOrder();
            for (Long order = curVersionOrder + 1; order <= lastNewestVersionOrder; order++) {
                Version changedVersion = this.versionDao.getOneVersionByProjectIdAndOrder(curProjectId, order);
                this.versionDao.updateOrderByVersion(changedVersion);
            }
            lastNewestVersion.setVersionOrder(lastNewestVersion.getVersionOrder() - 1);
            this.projectDao.updateTheNewestVersion(curProjectId, lastNewestVersion.getVersionId(), lastNewestVersion.getVersionOrder() - 1);
            //此处应该添加删除与该版本相关信息的代码
        }
        return "200";

    }


    public void modifyVersionName(Long versionId, String newVersionName) {
        Version version = new Version();
        version.setVersionId(versionId);
        version.setVersionName(newVersionName);
        this.versionDao.modifyVersionName(version);
    }


    public void modifyVersionInfo(Long versionId, String newVersionInfo) {
        Version version = new Version();
        version.setVersionId(versionId);
        version.setVersionInfo(newVersionInfo);
        this.versionDao.modifyVersionInfo(version);
    }


}
