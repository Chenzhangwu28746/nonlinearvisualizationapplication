package com.xmu.nonlinearvisualization.service;

import com.xmu.nonlinearvisualization.dao.PrototypeDao;
import com.xmu.nonlinearvisualization.entity.Prototype;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * prototype service
 *
 * @author ahua
 * @date 2020/1/7
 */
@Service
public class PrototypeService {
    private PrototypeDao prototypeDao;

    @Autowired
    PrototypeService(PrototypeDao prototypeDao) {
        this.prototypeDao = prototypeDao;
    }


    public List<Prototype> selectPrototypeById(Long prototypeId) {
        return prototypeDao.selectPrototypeById(prototypeId);
    }


}
