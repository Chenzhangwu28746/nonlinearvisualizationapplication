package com.xmu.nonlinearvisualization.service;


import com.xmu.nonlinearvisualization.dao.FigureDao;
import com.xmu.nonlinearvisualization.entity.Figure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * figure service
 *
 * @author ahua
 * @date 2020/1/28
 */
@Service
public class FigureService {

    private FigureDao figureDao;

    @Autowired
    FigureService(FigureDao figureDao) {
        this.figureDao = figureDao;
    }


    public List<Figure> getAllFigureByVersionId(Long versionId) {
        return this.figureDao.getAllFigureByVersionId(versionId);
    }
}
