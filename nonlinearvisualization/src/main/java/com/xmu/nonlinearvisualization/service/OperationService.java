package com.xmu.nonlinearvisualization.service;


import com.xmu.nonlinearvisualization.dao.OperationDao;
import com.xmu.nonlinearvisualization.entity.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * operation service
 *
 * @author ahua
 * @date 2020/1/28
 */
@Service
public class OperationService {
    private OperationDao operationDao;

    @Autowired
    OperationService(OperationDao operationDao) {
        this.operationDao = operationDao;
    }


    public List<Operation> getAllOperationByVersionId(Long versionId) {
        return this.operationDao.getAllOperationByVersionId(versionId);
    }

    public String updateAllOperationByVersionId(MultipartFile file, Long curVersionId) {
        return "200";

    }
}
