package com.xmu.nonlinearvisualization.service;

import com.xmu.nonlinearvisualization.dao.ProjectDao;
import com.xmu.nonlinearvisualization.dao.VersionDao;
import com.xmu.nonlinearvisualization.entity.Project;
import com.xmu.nonlinearvisualization.entity.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * project service
 *
 * @author ahua
 * @date 2020/4/18
 */
@Service
public class ProjectService {
    private ProjectDao projectDao;
    private VersionDao versionDao;

    @Autowired
    ProjectService(ProjectDao projectDao, VersionDao versionDao) {
        this.projectDao = projectDao;
        this.versionDao = versionDao;
    }

    public List<Project> selectAllProjectByAccount(Long account) {
        return this.projectDao.selectAllProjectByAccount(account);
    }

    public Long getTheNewestVersionId(Long projectId) {
        return this.projectDao.getTheNewestVersionId(projectId);
    }

    public void addNewProject(Long account, String projectName) {
        Project project = new Project();
        Calendar c = Calendar.getInstance();
        String projectId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND) + "1";

        Version version = new Version();
        String versionId = "" + c.get(Calendar.YEAR) + "" + c.get(Calendar.MONTH) +
                "" + c.get(Calendar.DATE) + "" + c.get(Calendar.HOUR_OF_DAY) + "" +
                c.get(Calendar.MINUTE) + "" + c.get(Calendar.SECOND) + "2";
        version.setVersionOrder(Long.valueOf(1));
        version.setProjectId(Long.valueOf(projectId));
        version.setVersionInfo("版本1");
        version.setVersionName("版本1");
        project.setDesignerAccount(account);
        project.setProjectName(projectName);
        project.setProjectInfo(projectName);
        project.setProjectId(Long.valueOf(projectId));
        project.setNewestVersionId(Long.valueOf(versionId));
        project.setNewestVersionOrder(Long.valueOf(1));
        this.projectDao.addNewProject(project);
        this.versionDao.addNewVersion(version);
    }

    public void modifyProjectName(Long projectId, String projectName) {
        Project project = new Project();
        project.setProjectId(projectId);
        project.setProjectName(projectName);
        this.projectDao.modifyProjectName(project);
    }

    public void modifyProjectInfo(Long projectId, String projectInfo) {
        Project project = new Project();
        project.setProjectId(projectId);
        project.setProjectInfo(projectInfo);
        this.projectDao.modifyProjectInfo(project);
    }
}
