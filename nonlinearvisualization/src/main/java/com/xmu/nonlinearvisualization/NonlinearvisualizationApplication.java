package com.xmu.nonlinearvisualization;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;


@SpringBootApplication
@MapperScan(basePackages = "com.xmu.nonlinearvisualization.mapper")
public class NonlinearvisualizationApplication {

    public static void main(String[] args) throws IOException {
       //System.out.println(System.currentTimeMillis());
        Thread mythread = new Thread(() -> {
            try {
                //1.创建服务端socket,并指定其绑定的端口
                ServerSocket serverSocket = new ServerSocket(8090);
                while (true){
                    //2.等待接收客户端的socket连接,此方法在收到客户端socket连接前一直是阻塞的
                    Socket socket = serverSocket.accept();
                    //3.获取已经建立连接的socket通道的输出流和输入流
                    InputStream in = socket.getInputStream();
                    OutputStream out = socket.getOutputStream();
                    byte[] buf = new byte[1024];
                    int len = 0;
                    len = in.read(buf);
                    String AndroidMsg = new String(buf, 0, len);
                    //4.利用socket输出流向客户端发送数据
                    out.write("服务器端返回数据".getBytes());
                    out.flush();
                    //5.关闭资源
                    socket.close();
                    //serverSocket.close();
                    System.out.println("AndroidMsg：" + AndroidMsg);
                    String url = "http://127.0.0.1:8080/getAndroidValue";//路径一定要对否则通不过
                    // 创建默认的httpClient实例.
                    HttpClient client = new DefaultHttpClient();
                    // 创建httppost通过post方式访问
                    HttpPost httppost = new HttpPost(url);
                    //作为参数发送到controller
                    StringEntity entity = new StringEntity(AndroidMsg, "utf-8");
                    //StringEntity entity = new StringEntity("main方法传来的消息！","utf-8");
                    entity.setContentEncoding("UTF-8");
                    entity.setContentType("application/json");
                    httppost.setEntity(entity);
                    HttpResponse response = client.execute(httppost);
                    String result = "";
                    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        result += EntityUtils.toString(response.getEntity());
                        System.out.println("已传输给AndroidController的值：" + result);
                    } else {
                        result = "请求失败";
                        System.out.println(result);
                    }
                }
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });
        mythread.start();
        System.out.println(mythread.getState());


        SpringApplication.run(NonlinearvisualizationApplication.class, args);


    }

}
