var shaderVertexSrc = "\n" +
"precision mediump float;" +
"\n" +
"attribute vec3      a_Position;\n" +
"attribute vec3      a_Normal;\n" +
"attribute vec2      a_TimeAlive;\n" +
"\n" +
"varying   vec3      v_Position;\n" +
"varying   vec3      v_Normal;\n" +
"varying   vec3      v_Color;\n" +
"\n" +
"uniform   float     u_TimeCurrent;\n" +
"\n" +
"uniform   vec3      u_LightAmbient;\n" +
"uniform   vec3      u_LightColor0;\n" +
"uniform   vec3      u_LightDirection0;\n" +
"uniform   vec3      u_LightColor1;\n" +
"uniform   vec3      u_LightDirection1;\n" +
"uniform   vec3      u_LightColor2;\n" +
"uniform   vec3      u_LightDirection2;\n" +
"uniform   vec3      u_LightColor3;\n" +
"uniform   vec3      u_LightDirection3;\n" +
"\n" +
"uniform   mat4      u_MMatrix;\n" +
"uniform   mat4      u_VMatrix;\n" +
"uniform   mat4      u_PMatrix;\n" +
"uniform   mat4      u_NMatrix;\n" +
"\n" +
"\n" +
"void main() {\n" +
"    vec4 v = u_VMatrix * u_MMatrix * vec4(a_Position,1);\n" +
"    vec4 n = u_NMatrix * vec4(a_Normal, 0);\n" +
"    \n" +
"    gl_Position = u_PMatrix * v;\n" +
"    v_Normal    = normalize(n.xyz);\n" +
"    v_Position  = v.xyz / v.w;\n" +
"    v_Color     = vec3(1.0,1.0,1.0);\n" +
"    \n" +
"    if(u_TimeCurrent < a_TimeAlive.x - 0.01 || u_TimeCurrent > a_TimeAlive.y - 0.01) {\n" +
"        gl_Position.z = -100.0;\n" +
"    }\n" +
"    if(u_TimeCurrent < a_TimeAlive.x + 0.99) {\n" +
"        v_Color = vec3(0.7,1.5,0.7);\n" +
"    }\n" +
"}\n";


var shaderFragmentSrc = "\n" +
"precision mediump float;\n" +
"\n" +
"varying   vec3      v_Position;\n" +
"varying   vec3      v_Normal;\n" +
"varying   vec3      v_Color;\n" +
"\n" +
"uniform   float     u_TimeCurrent;\n" +
"\n" +
"uniform   vec3      u_LightAmbient;\n" +
"uniform   vec3      u_LightColor0;\n" +
"uniform   vec3      u_LightDirection0;\n" +
"uniform   vec3      u_LightColor1;\n" +
"uniform   vec3      u_LightDirection1;\n" +
"uniform   vec3      u_LightColor2;\n" +
"uniform   vec3      u_LightDirection2;\n" +
"uniform   vec3      u_LightColor3;\n" +
"uniform   vec3      u_LightDirection3;\n" +
"\n" +
"uniform   mat4      u_MMatrix;\n" +
"uniform   mat4      u_VMatrix;\n" +
"uniform   mat4      u_PMatrix;\n" +
"uniform   mat4      u_NMatrix;\n" +
"\n" +
"vec3 compute_light(vec3 light_color, vec3 light_dir, vec3 poly_norm) {\n" +
"    vec4 d = normalize(u_VMatrix * vec4(poly_norm,0));\n" +
"    return light_color * abs(dot(light_dir, d.xyz));\n" +
"}\n" +
"\n" +
"void main() {\n" +
"    vec3 matDiffuse = vec3(1.0, 1.0, 1.0); //0.7,0.7,0.7);\n" +
"    \n" +
"    vec3 diffuse0 = compute_light(u_LightColor0, u_LightDirection0, v_Normal);\n" +
"    vec3 diffuse1 = compute_light(u_LightColor1, u_LightDirection1, v_Normal);\n" +
"    vec3 diffuse2 = compute_light(u_LightColor2, u_LightDirection2, v_Normal);\n" +
"    vec3 diffuse3 = compute_light(u_LightColor3, u_LightDirection3, v_Normal);\n" +
"    vec3 diffuse = (diffuse0 + diffuse1 + diffuse2 + diffuse3) * matDiffuse;\n" +
"    vec3 ambient = u_LightAmbient * matDiffuse;\n" +
"    \n" +
"    gl_FragColor = vec4(v_Color * (diffuse + ambient), 1.0);\n" +
"}\n";
