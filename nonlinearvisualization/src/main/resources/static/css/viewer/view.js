function report(txt) {
    document.getElementById('loading').innerHTML += '<br/>' + txt;
}
function report_sub(txt) {
    report('&nbsp;&nbsp;&nbsp;&nbsp;' + txt);
}

// http://stackoverflow.com/questions/15313418/javascript-assert
function assert(condition, message) {
    if (!condition) {
        message = message || "Assertion failed";
        report('<span style="color:red;">! ' + message + ' !</span>');
        if (typeof Error !== "undefined") {
            throw new Error(message);
        }
        throw message; // Fallback
    }
}



var loading_left = 4;
var fadeout_timer = null;
var fadeout_per = 50.0;
function loading_done(t) {
    report_sub(t + ' loaded');
    loading_left--;
    if(loading_left != 0) return;
    
    report_sub('success!');
    
    init_webgl();
    
    setup_lights();
    
    init_mesh();
    
    init_level(0);
    report('mesh.time_max = ' + mesh.time_max);
    
    add_ui_fields();
    
    render();
    
    fadeout_timer = setInterval("fadeout_loading();", 10);
}

function fadeout_loading() {
    fadeout_per *= 0.98;
    document.getElementById('loading').style.color = "rgba(255,255,255," + Math.min(fadeout_per,1) + ")";
    if(fadeout_per < 0.20) {
        //document.getElementById('loading').style.display = "none";
        window.clearInterval(fadeout_timer);
    }
}


var use_artist_camera = false;


var canvas = null;
var gl = null;

var shader = {};
var matrices = {};
var mesh = {};

var mouse = {
    down: false,    // is button pressed?
    button: 0,      // last button pressed
     x: 0,  y: 0,   // current mouse position
    ox: 0, oy: 0,   // previous mouse position
    dx: 0, dy: 0,   // mouse position when mouse was pressed
    wheel: 0,       // current wheel position
    owheel: 0,      // previous wheel position
};

var keyboard = {
    keyCode:    0,
    which:      0,
    charCode:   0,
    charString: '',
    shiftKey:   false,
    ctrlKey:    false,
    altKey:     false,
    metaKey:    false,
    
    keyPress:   '',
};

var camera = new Camera();

window.onload = function() {
    //main();
}


function init_webgl() {
    canvas = document.getElementById("gl-canvas");
    hook_mouse();
    hook_keyboard();
    
    
    report('initializing WebGL');
    gl = WebGLUtils.setupWebGL(canvas);
    assert(gl, "WebGL isn't available");
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);
    report_sub('success!');
    
    
    report('initializing shaders');
    //var shaderVertexSrc   = document.getElementById('shadervert').innerHTML;
    //var shaderFragmentSrc = document.getElementById('shaderfrag').innerHTML;
    shader.program = createProgram(gl, shaderVertexSrc, shaderFragmentSrc);
    assert(shader.program, "could not create shader program");
    report_sub('created');
    
    shader.a_Position  = gl.getAttribLocation(shader.program, 'a_Position');
    shader.a_Normal    = gl.getAttribLocation(shader.program, 'a_Normal');
    shader.a_TimeAlive = gl.getAttribLocation(shader.program, 'a_TimeAlive');
    shader.u_TimeCurrent = gl.getUniformLocation(shader.program, 'u_TimeCurrent');
    shader.u_MMatrix = gl.getUniformLocation(shader.program, 'u_MMatrix');
    shader.u_VMatrix = gl.getUniformLocation(shader.program, 'u_VMatrix');
    shader.u_PMatrix = gl.getUniformLocation(shader.program, 'u_PMatrix');
    shader.u_NMatrix = gl.getUniformLocation(shader.program, 'u_NMatrix');
    shader.u_LightAmbient    = gl.getUniformLocation(shader.program, 'u_LightAmbient');
    shader.u_LightColor0     = gl.getUniformLocation(shader.program, 'u_LightColor0');
    shader.u_LightDirection0 = gl.getUniformLocation(shader.program, 'u_LightDirection0');
    shader.u_LightColor1     = gl.getUniformLocation(shader.program, 'u_LightColor1');
    shader.u_LightDirection1 = gl.getUniformLocation(shader.program, 'u_LightDirection1');
    shader.u_LightColor2     = gl.getUniformLocation(shader.program, 'u_LightColor2');
    shader.u_LightDirection2 = gl.getUniformLocation(shader.program, 'u_LightDirection2');
    shader.u_LightColor3     = gl.getUniformLocation(shader.program, 'u_LightColor3');
    shader.u_LightDirection3 = gl.getUniformLocation(shader.program, 'u_LightDirection3');
    
    assert(shader.a_Position    != -1, "could not find a_Position");
    assert(shader.a_Normal      != -1, "could not find a_Normal");
    assert(shader.a_TimeAlive   != -1, "could not find a_TimeAlive");
    assert(shader.u_TimeCurrent != -1, "could not find u_TimeCurrent");
    assert(shader.u_MMatrix     != -1, "could not find u_MMatrix");
    assert(shader.u_VMatrix     != -1, "could not find u_VMatrix");
    assert(shader.u_PMatrix     != -1, "could not find u_PMatrix");
    assert(shader.u_NMatrix     != -1, "could not find u_NMatrix");
    assert(shader.u_LightAmbient    != -1, "could not find u_LightAmbient");
    assert(shader.u_LightColor0     != -1, "could not find u_LightColor0");
    assert(shader.u_LightDirection0 != -1, "could not find u_LightDirection0");
    assert(shader.u_LightColor1     != -1, "could not find u_LightColor1");
    assert(shader.u_LightDirection1 != -1, "could not find u_LightDirection1");
    assert(shader.u_LightColor2     != -1, "could not find u_LightColor2");
    assert(shader.u_LightDirection2 != -1, "could not find u_LightDirection2");
    assert(shader.u_LightColor3     != -1, "could not find u_LightColor3");
    assert(shader.u_LightDirection3 != -1, "could not find u_LightDirection3");
    
    report_sub('variables located!');
    
    gl.useProgram(shader.program);
    report_sub('success!');
    
    
    report('initializing matrices');
    // Calculate the view projection matrix
    matrices.m = new Matrix4();
    matrices.v = new Matrix4();
    matrices.p = new Matrix4();
    matrices.n = new Matrix4();
    
    matrices.p.setPerspective(30, canvas.width/canvas.height, 0.1, 10);
    matrices.v.setLookAt(1,1,2,  0,0,0,  0,1,0);
    matrices.m.setIdentity();
    matrices.n.setIdentity();
    
    // Pass the model view projection matrix to the variable u_MvpMatrix
    gl.uniformMatrix4fv(shader.u_VMatrix, false, matrices.v.elements);
    gl.uniformMatrix4fv(shader.u_PMatrix, false, matrices.p.elements);
    gl.uniformMatrix4fv(shader.u_MMatrix, false, matrices.m.elements);
    gl.uniformMatrix4fv(shader.u_NMatrix, false, matrices.n.elements);
    report_sub('success!');
    
    
    //add_ui_fields();
    
    window.addEventListener('resize', resize_context);
    resize_context();
}

function init_mesh() {
    report('initializing mesh');
    
    vert_pos  = new Array();
    vert_norm = new Array();
    for(i_tri = 0; i_tri < tris.length; i_tri += 3) {
        i_v0 = tris[i_tri+0];
        i_v1 = tris[i_tri+1];
        i_v2 = tris[i_tri+2];
        v0 = [verts[i_v0*3+0], verts[i_v0*3+1], verts[i_v0*3+2]];
        v1 = [verts[i_v1*3+0], verts[i_v1*3+1], verts[i_v1*3+2]];
        v2 = [verts[i_v2*3+0], verts[i_v2*3+1], verts[i_v2*3+2]];
        v01 = new Vector3([v1[0]-v0[0],v1[1]-v0[1],v1[2]-v0[2]]);
        v02 = new Vector3([v2[0]-v0[0],v2[1]-v0[1],v2[2]-v0[2]]);
        n = v01.crossed(v02).normalize();
        vert_pos.push(v0[0]); vert_pos.push(v0[1]); vert_pos.push(v0[2]);
        vert_pos.push(v1[0]); vert_pos.push(v1[1]); vert_pos.push(v1[2]);
        vert_pos.push(v2[0]); vert_pos.push(v2[1]); vert_pos.push(v2[2]);
        vert_norm.push(n.x()); vert_norm.push(n.y()); vert_norm.push(n.z());
        vert_norm.push(n.x()); vert_norm.push(n.y()); vert_norm.push(n.z());
        vert_norm.push(n.x()); vert_norm.push(n.y()); vert_norm.push(n.z());
    }
    report_sub('triangles created');
    
    mesh.vert_count = tris.length;
    mesh.i_level = -1;
    mesh.i_time = -1;
    mesh.time_max = 0;
    mesh.level_max = levels.length-1;
    
    mesh.buffer_pos   = gl.createBuffer();
    mesh.buffer_norm  = gl.createBuffer();
    mesh.buffer_alive = gl.createBuffer();
    assert(mesh.buffer_pos,   "could not create pos buffer");
    assert(mesh.buffer_norm,  "could not create norm buffer");
    assert(mesh.buffer_alive, "could not create alive buffer");
    report_sub('buffers created');
    
    
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.buffer_pos);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vert_pos), gl.STATIC_DRAW);
    gl.vertexAttribPointer(shader.a_Position, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shader.a_Position);
    
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.buffer_norm);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vert_norm), gl.STATIC_DRAW);
    gl.vertexAttribPointer(shader.a_Normal, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shader.a_Normal);
    
    report_sub('data buffered');
    
    report_sub('success!');
}

function init_level(i_level) {
    if(mesh.i_level == i_level) return;
    mesh.i_level  = i_level;
    mesh.time_max = 0;
    mesh.i_time = -1;
    
    report('initializing level ' + i_level);
    alive = new Array(mesh.vert_count*2);
    for(var i = 0; i < mesh.vert_count; i++) {
        alive[i*6+0] = -1;
        alive[i*6+1] = -1;
        alive[i*6+2] = -1;
        alive[i*6+3] = -1;
        alive[i*6+4] = -1;
        alive[i*6+5] = -1;
    }
    level = levels[i_level];
    for(var i_start in level) {
        if(parseInt(i_start) > mesh.time_max) { mesh.time_max = parseInt(i_start); }
        var l = level[i_start];
        for(var i_end in l) {
            var e = l[i_end];
            for(var idx in e) {
                var dist = parseInt(e[idx]);
                alive[idx*6+0] = parseFloat(i_start);
                alive[idx*6+1] = parseFloat(i_end);
                alive[idx*6+2] = parseFloat(i_start);
                alive[idx*6+3] = parseFloat(i_end);
                alive[idx*6+4] = parseFloat(i_start);
                alive[idx*6+5] = parseFloat(i_end);
            }
        }
    }
    report_sub('alive generated');
    
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.buffer_alive);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(alive), gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(shader.a_TimeAlive, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(shader.a_TimeAlive);
    report_sub('data buffered');
    
    report_sub('success!');
}



function make_normal(v) {
    l = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
    return [v[0] / l, v[1] / l, v[2] / l];
}
function setup_lights() {
    gl.uniform3f(shader.u_LightAmbient, 0.25, 0.25, 0.25);
    
    gl.uniform3f(shader.u_LightColor0, 0.30, 0.30, 0.32);
    gl.uniform3fv(shader.u_LightDirection0, make_normal([1.0,1.0,1.0]));
    gl.uniform3f(shader.u_LightColor1, 0.22, 0.20, 0.20);
    gl.uniform3fv(shader.u_LightDirection1, make_normal([-1.0,1.0,1.0]));
    gl.uniform3f(shader.u_LightColor2, 0.10, 0.10, 0.10);
    gl.uniform3fv(shader.u_LightDirection2, make_normal([-1.0,-0.2,-1.0]));
    gl.uniform3f(shader.u_LightColor0, 0.10, 0.07, 0.10);
    gl.uniform3fv(shader.u_LightDirection0, make_normal([0.0,0.0,-1.0]));
}

var render_opts = {
    timelevel: 0,
};
function render() {
    if(keyboard.keyPressType > 0) {
        timenow = Date.now();
        if(keyboard.keyCode == 37) {
            document.getElementById('time').value--;
        }
        if(keyboard.keyCode == 39) {
            document.getElementById('time').value++;
        }
        if(keyboard.keyCode == 38 && timenow - render_opts.timelevel > 250) {
            document.getElementById('level').value++;
            render_opts.timelevel = timenow;
            update_level();
        }
        if(keyboard.keyCode == 40 && timenow - render_opts.timelevel > 250) {
            document.getElementById('level').value--;
            render_opts.timelevel = timenow;
            update_level();
        }
    }
    
    var time_current  = document.getElementById('time').value;
    if(mesh.i_time != time_current && typeof extra !== 'undefined') {
        mesh.i_time = time_current;
        d = levels[mesh.i_level][mesh.i_time];
        
        commands = [];
        counts = {};
        for(var idx = d.i; idx < d.i+d.l; idx++) {
            cur = extra[idx].op;
            if(!(cur in counts)) {
                counts[cur] = 0;
                commands.push(cur);
            }
            counts[cur]++;
        }
        commands.sort();
        
        html = '';
        html += '<table>';
        html += '<tr class="header"><td class="count">' + d.l + '</td><td class="command">operation' + (d.l==1?'':'s') + '</td></tr>';
        for(var i = 0; i < commands.length; i++) {
            cmd = commands[i];
            html += '<tr><td class="count">' + counts[cmd] + '</td><td class="command">' + cmd + '</td></tr>';
        }
        html += '</table>'
        document.getElementById('command').innerHTML = html;
    }
    
    if(mouse.down) {
        if(keyboard.shiftKey) {
            camera.pan(mouse.ox - mouse.x, mouse.y - mouse.oy);
        } else if(keyboard.ctrlKey) {
            dy = (mouse.oy-mouse.y) / 10.0;
            camera.dolly(Math.pow(2.0, dy));
        } else {
            camera.rotate_axisangle(camera.fx, mouse.oy - mouse.y);
            camera.rotate_axisangle(new Vec3([0,0,1]), mouse.ox - mouse.x);
            //camera.rotate_axisangle(new Vec3([1,0,0]), mouse.oy - mouse.y);
        }
    }
    if(mouse.wheel != mouse.owheel) {
        if(mouse.wheel < mouse.owheel) {
            camera.dolly(1.2);
        } else {
            camera.dolly(1.0 / 1.2);
        }
    }
    mouse.ox = mouse.x;
    mouse.oy = mouse.y;
    mouse.owheel = mouse.wheel;
    
    if(typeof extra !== 'undefined' && use_artist_camera && document.getElementById('camera').checked) {
        var ex = extra[time_current];
        camera.fo = new Vec3(ex.camera.o);
        camera.fx = new Vec3(ex.camera.x);
        camera.fy = new Vec3(ex.camera.y);
        camera.fz = new Vec3(ex.camera.z);
        camera.fd = Math.max(0.001, ex.camera.d)*10;
    }
    
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    
    gl.uniformMatrix4fv(shader.u_VMatrix, false, camera.viewmatrix());
    
    gl.uniform1f(shader.u_TimeCurrent, time_current);
    
    // draw first object
    if(false) {
        matrices.m.setIdentity();
        matrices.m.rotate(-90, 1, 0, 0);
        matrices.m.rotate(180, 0, 0, 1);
        matrices.n.setInverseOf(matrices.m);
        matrices.n.transpose();
    }
    gl.uniformMatrix4fv(shader.u_MMatrix, false, matrices.m.elements);
    gl.uniformMatrix4fv(shader.u_NMatrix, false, matrices.n.elements);
    
    gl.drawArrays(gl.TRIANGLES, 0, mesh.vert_count);
    
    requestAnimationFrame(render, canvas);
}



// Rotation angle (degrees/second)
var ANGLE_STEP = 10.0;
// Last time that this function was called
var g_last = Date.now();
function animate(angle) {
    // Calculate the elapsed time
    var now = Date.now();
    var elapsed = now - g_last;
    g_last = now;
    // Update the current rotation angle (adjusted by the elapsed time)
    var newAngle = angle + (ANGLE_STEP * elapsed) / 1000.0;
    return newAngle;
}



// http://codepen.io/Astralized/pen/xanrB
function hook_mouse() {
    // add mouse wheel handling (dolly in/out)
    canvas.onmousewheel = function(e) {
        var wheel = e.wheelDelta / 120;//n or -n
        mouse.wheel += wheel;
        e.preventDefault();
    };
    
    canvas.onmousemove = function(e) {
        mouse.x  = e.pageX - canvas.offsetLeft,
        mouse.y  = e.pageY - canvas.offsetTop;
        e.preventDefault();
    };

    canvas.onmousedown = function(e) {
        mouse.button = e.which;
        mouse.down   = true;
        mouse.dx = mouse.x;
        mouse.dy = mouse.y;
        e.preventDefault();
    };

    canvas.oncontextmenu = function(e) {
        e.preventDefault();
    };

    canvas.onmouseup = function(e) {
        mouse.down = false;
        e.preventDefault();
    };
}

function hook_keyboard() {
    function handler(e,d) {
        keyboard.keyPressType = d;
        keyboard.keyCode = e.keyCode;
        keyboard.which = e.which;
        keyboard.charCode = e.charCode;
        keyboard.charString = String.fromCharCode(e.keyCode || e.charCode);
        keyboard.shiftKey = e.shiftKey;
        keyboard.ctrlKey = e.ctrlKey;
        keyboard.altKey = e.altKey;
        keyboard.metaKey = e.metaKey;
        /*var s = "dup"[t] + ' ' + keyCode + ' ' + which + ' '
                + charCode + ' ' + charString + ' '
                + (shiftKey?'1':'0') + (ctrlKey?'1':'0')
                + (altKey?'1':'0') + (metaKey?'1':'0');*/
        //return true;   // return false to prevent key pass-through
        return false;
    }
    document.onkeydown  = function(e) { return handler(e,1); };
    document.onkeyup    = function(e) { return handler(e,0); };
    document.onkeypress = function(e) { return handler(e,2); };
}


function update_level() {
    var i_level = document.getElementById('level').value;
    init_level(i_level);
    
    document.getElementById('time').max = levels[i_level].length-1;
    document.getElementById('time').value = document.getElementById('time').value;
}

function add_ui_fields() {
    var options = document.getElementById("options");
    
    var html = '';
    html += '<table>';
    
    html += create_ui_range('time', 'step', 0, mesh.time_max, mesh.time_max);
    html += create_ui_range_onchange('level', 'LoD', 0, mesh.level_max, 0, "update_level()");
    if(typeof extra !== 'undefined' && use_artist_camera) {
        html += create_ui_checkbox('camera', 'artist camera', false);
    }
    
    html += '</table>';
    
    
    options.innerHTML = html;
    
    var src = document.getElementById("source");
    src.innerHTML = "LMB: rotate<br />CTRL+LMB: dolly<br />SHIFT+LMB: pan<br />" + src.innerHTML;
    
}

function resize_context() {
    canvas.width  = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    gl.viewport(0, 0, canvas.width, canvas.height);
    matrices.p.setPerspective(30, canvas.width/canvas.height, 0.01, 20);
    gl.uniformMatrix4fv(shader.u_PMatrix, false, matrices.p.elements);
}


