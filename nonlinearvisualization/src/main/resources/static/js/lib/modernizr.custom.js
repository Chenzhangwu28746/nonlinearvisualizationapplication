/**
 * @license Modernizr 2.6.2 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-touch-shiv-mq-teststyles-prefixes-load
 */
;


window.Modernizr = (function (window, document, undefined) {

    var version = '2.6.2',

        Modernizr = {},


        docElement = document.documentElement,

        mod = 'modernizr',
        modElem = document.createElement(mod),
        mStyle = modElem.style,

        inputElem,


        toString = {}.toString,

        prefixes = ' -webkit- -moz- -o- -ms- '.split(' '),


        tests = {},
        inputs = {},
        attrs = {},

        classes = [],

        slice = classes.slice,

        featureName,


        injectElementWithStyles = function (rule, callback, nodes, testnames) {

            var style, ret, node, docOverflow,
                div = document.createElement('div'),
                body = document.body,
                fakeBody = body || document.createElement('body');

            if (parseInt(nodes, 10)) {
                while (nodes--) {
                    node = document.createElement('div');
                    node.id = testnames ? testnames[nodes] : mod + (nodes + 1);
                    div.appendChild(node);
                }
            }

            style = ['&#173;', '<style id="s', mod, '">', rule, '</style>'].join('');
            div.id = mod;
            (body ? div : fakeBody).innerHTML += style;
            fakeBody.appendChild(div);
            if (!body) {
                fakeBody.style.background = '';
                fakeBody.style.overflow = 'hidden';
                docOverflow = docElement.style.overflow;
                docElement.style.overflow = 'hidden';
                docElement.appendChild(fakeBody);
            }

            ret = callback(div, rule);
            if (!body) {
                fakeBody.parentNode.removeChild(fakeBody);
                docElement.style.overflow = docOverflow;
            } else {
                div.parentNode.removeChild(div);
            }

            return !!ret;

        },

        testMediaQuery = function (mq) {

            var matchMedia = window.matchMedia || window.msMatchMedia;
            if (matchMedia) {
                return matchMedia(mq).matches;
            }

            var bool;

            injectElementWithStyles('@media ' + mq + ' { #' + mod + ' { position: absolute; } }', function (node) {
                bool = (window.getComputedStyle ?
                    getComputedStyle(node, null) :
                    node.currentStyle)['position'] == 'absolute';
            });

            return bool;

        },
        _hasOwnProperty = ({}).hasOwnProperty, hasOwnProp;

    if (!is(_hasOwnProperty, 'undefined') && !is(_hasOwnProperty.call, 'undefined')) {
        hasOwnProp = function (object, property) {
            return _hasOwnProperty.call(object, property);
        };
    } else {
        hasOwnProp = function (object, property) {
            return ((property in object) && is(object.constructor.prototype[property], 'undefined'));
        };
    }


    if (!Function.prototype.bind) {
        Function.prototype.bind = function bind(that) {

            var target = this;

            if (typeof target != "function") {
                throw new TypeError();
            }

            var args = slice.call(arguments, 1),
                bound = function () {

                    if (this instanceof bound) {

                        var F = function () {
                        };
                        F.prototype = target.prototype;
                        var self = new F();

                        var result = target.apply(
                            self,
                            args.concat(slice.call(arguments))
                        );
                        if (Object(result) === result) {
                            return result;
                        }
                        return self;

                    } else {

                        return target.apply(
                            that,
                            args.concat(slice.call(arguments))
                        );

                    }

                };

            return bound;
        };
    }

    function setCss(str) {
        mStyle.cssText = str;
    }

    function setCssAll(str1, str2) {
        return setCss(prefixes.join(str1 + ';') + (str2 || ''));
    }

    function is(obj, type) {
        return typeof obj === type;
    }

    function contains(str, substr) {
        return !!~('' + str).indexOf(substr);
    }


    function testDOMProps(props, obj, elem) {
        for (var i in props) {
            var item = obj[props[i]];
            if (item !== undefined) {

                if (elem === false) return props[i];

                if (is(item, 'function')) {
                    return item.bind(elem || obj);
                }

                return item;
            }
        }
        return false;
    }

    tests['touch'] = function () {
        var bool;

        if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
            bool = true;
        } else {
            injectElementWithStyles(['@media (', prefixes.join('touch-enabled),('), mod, ')', '{#modernizr{top:9px;position:absolute}}'].join(''), function (node) {
                bool = node.offsetTop === 9;
            });
        }

        return bool;
    };
    for (var feature in tests) {
        if (hasOwnProp(tests, feature)) {
            featureName = feature.toLowerCase();
            Modernizr[featureName] = tests[feature]();

            classes.push((Modernizr[featureName] ? '' : 'no-') + featureName);
        }
    }


    Modernizr.addTest = function (feature, test) {
        if (typeof feature == 'object') {
            for (var key in feature) {
                if (hasOwnProp(feature, key)) {
                    Modernizr.addTest(key, feature[key]);
                }
            }
        } else {

            feature = feature.toLowerCase();

            if (Modernizr[feature] !== undefined) {
                return Modernizr;
            }

            test = typeof test == 'function' ? test() : test;

            if (typeof enableClasses !== "undefined" && enableClasses) {
                docElement.className += ' ' + (test ? '' : 'no-') + feature;
            }
            Modernizr[feature] = test;

        }

        return Modernizr;
    };


    setCss('');
    modElem = inputElem = null;

    ;(function (window, document) {
        var options = window.html5 || {};

        var reSkip = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i;

        var saveClones = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i;

        var supportsHtml5Styles;

        var expando = '_html5shiv';

        var expanID = 0;

        var expandoData = {};

        var supportsUnknownElements;

        (function () {
            try {
                var a = document.createElement('a');
                a.innerHTML = '<xyz></xyz>';
                supportsHtml5Styles = ('hidden' in a);

                supportsUnknownElements = a.childNodes.length == 1 || (function () {
                    (document.createElement)('a');
                    var frag = document.createDocumentFragment();
                    return (
                        typeof frag.cloneNode == 'undefined' ||
                        typeof frag.createDocumentFragment == 'undefined' ||
                        typeof frag.createElement == 'undefined'
                    );
                }());
            } catch (e) {
                supportsHtml5Styles = true;
                supportsUnknownElements = true;
            }

        }());

        function addStyleSheet(ownerDocument, cssText) {
            var p = ownerDocument.createElement('p'),
                parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

            p.innerHTML = 'x<style>' + cssText + '</style>';
            return parent.insertBefore(p.lastChild, parent.firstChild);
        }

        function getElements() {
            var elements = html5.elements;
            return typeof elements == 'string' ? elements.split(' ') : elements;
        }

        function getExpandoData(ownerDocument) {
            var data = expandoData[ownerDocument[expando]];
            if (!data) {
                data = {};
                expanID++;
                ownerDocument[expando] = expanID;
                expandoData[expanID] = data;
            }
            return data;
        }

        function createElement(nodeName, ownerDocument, data) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            if (supportsUnknownElements) {
                return ownerDocument.createElement(nodeName);
            }
            if (!data) {
                data = getExpandoData(ownerDocument);
            }
            var node;

            if (data.cache[nodeName]) {
                node = data.cache[nodeName].cloneNode();
            } else if (saveClones.test(nodeName)) {
                node = (data.cache[nodeName] = data.createElem(nodeName)).cloneNode();
            } else {
                node = data.createElem(nodeName);
            }

            return node.canHaveChildren && !reSkip.test(nodeName) ? data.frag.appendChild(node) : node;
        }

        function createDocumentFragment(ownerDocument, data) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            if (supportsUnknownElements) {
                return ownerDocument.createDocumentFragment();
            }
            data = data || getExpandoData(ownerDocument);
            var clone = data.frag.cloneNode(),
                i = 0,
                elems = getElements(),
                l = elems.length;
            for (; i < l; i++) {
                clone.createElement(elems[i]);
            }
            return clone;
        }

        function shivMethods(ownerDocument, data) {
            if (!data.cache) {
                data.cache = {};
                data.createElem = ownerDocument.createElement;
                data.createFrag = ownerDocument.createDocumentFragment;
                data.frag = data.createFrag();
            }


            ownerDocument.createElement = function (nodeName) {
                if (!html5.shivMethods) {
                    return data.createElem(nodeName);
                }
                return createElement(nodeName, ownerDocument, data);
            };

            ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
                'var n=f.cloneNode(),c=n.createElement;' +
                'h.shivMethods&&(' +
                getElements().join().replace(/\w+/g, function (nodeName) {
                    data.createElem(nodeName);
                    data.frag.createElement(nodeName);
                    return 'c("' + nodeName + '")';
                }) +
                ');return n}'
            )(html5, data.frag);
        }

        function shivDocument(ownerDocument) {
            if (!ownerDocument) {
                ownerDocument = document;
            }
            var data = getExpandoData(ownerDocument);

            if (html5.shivCSS && !supportsHtml5Styles && !data.hasCSS) {
                data.hasCSS = !!addStyleSheet(ownerDocument,
                    'article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}' +
                    'mark{background:#FF0;color:#000}'
                );
            }
            if (!supportsUnknownElements) {
                shivMethods(ownerDocument, data);
            }
            return ownerDocument;
        }

        var html5 = {

            'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video',

            'shivCSS': (options.shivCSS !== false),

            'supportsUnknownElements': supportsUnknownElements,

            'shivMethods': (options.shivMethods !== false),

            'type': 'default',

            'shivDocument': shivDocument,

            createElement: createElement,

            createDocumentFragment: createDocumentFragment
        };
        window.html5 = html5;

        shivDocument(document);

    }(this, document));

    Modernizr._version = version;

    Modernizr._prefixes = prefixes;

    Modernizr.mq = testMediaQuery;
    Modernizr.testStyles = injectElementWithStyles;
    return Modernizr;

})(this, this.document);

Modernizr.load = function () {
    yepnope.apply(window, [].slice.call(arguments, 0));
};
;
// SIG // Begin signature block
// SIG // MIIepgYJKoZIhvcNAQcCoIIelzCCHpMCAQExCzAJBgUr
// SIG // DgMCGgUAMGcGCisGAQQBgjcCAQSgWTBXMDIGCisGAQQB
// SIG // gjcCAR4wJAIBAQQQEODJBs441BGiowAQS9NQkAIBAAIB
// SIG // AAIBAAIBAAIBADAhMAkGBSsOAwIaBQAEFMWO6HF5KJg1
// SIG // qQoqFPMkyHQZNr+0oIIZjTCCA+4wggNXoAMCAQICEH6T
// SIG // 6/t8xk5Z6kuad9QG/DswDQYJKoZIhvcNAQEFBQAwgYsx
// SIG // CzAJBgNVBAYTAlpBMRUwEwYDVQQIEwxXZXN0ZXJuIENh
// SIG // cGUxFDASBgNVBAcTC0R1cmJhbnZpbGxlMQ8wDQYDVQQK
// SIG // EwZUaGF3dGUxHTAbBgNVBAsTFFRoYXd0ZSBDZXJ0aWZp
// SIG // Y2F0aW9uMR8wHQYDVQQDExZUaGF3dGUgVGltZXN0YW1w
// SIG // aW5nIENBMB4XDTEyMTIyMTAwMDAwMFoXDTIwMTIzMDIz
// SIG // NTk1OVowXjELMAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5
// SIG // bWFudGVjIENvcnBvcmF0aW9uMTAwLgYDVQQDEydTeW1h
// SIG // bnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIENBIC0g
// SIG // RzIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
// SIG // AQCxrLNJVEuXHBIK2CV5kSJXKm/cuCbEQ3Nrwr8uUFr7
// SIG // FMJ2jkMBJUO0oeJF9Oi3e8N0zCLXtJQAAvdN7b+0t0Qk
// SIG // a81fRTvRRM5DEnMXgotptCvLmR6schsmTXEfsTHd+1Fh
// SIG // AlOmqvVJLAV4RaUvic7nmef+jOJXPz3GktxK+Hsz5HkK
// SIG // +/B1iEGc/8UDUZmq12yfk2mHZSmDhcJgFMTIyTsU2sCB
// SIG // 8B8NdN6SIqvK9/t0fCfm90obf6fDni2uiuqm5qonFn1h
// SIG // 95hxEbziUKFL5V365Q6nLJ+qZSDT2JboyHylTkhE/xni
// SIG // RAeSC9dohIBdanhkRc1gRn5UwRN8xXnxycFxAgMBAAGj
// SIG // gfowgfcwHQYDVR0OBBYEFF+a9W5czMx0mtTdfe8/2+xM
// SIG // gC7dMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcwAYYW
// SIG // aHR0cDovL29jc3AudGhhd3RlLmNvbTASBgNVHRMBAf8E
// SIG // CDAGAQH/AgEAMD8GA1UdHwQ4MDYwNKAyoDCGLmh0dHA6
// SIG // Ly9jcmwudGhhd3RlLmNvbS9UaGF3dGVUaW1lc3RhbXBp
// SIG // bmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYD
// SIG // VR0PAQH/BAQDAgEGMCgGA1UdEQQhMB+kHTAbMRkwFwYD
// SIG // VQQDExBUaW1lU3RhbXAtMjA0OC0xMA0GCSqGSIb3DQEB
// SIG // BQUAA4GBAAMJm495739ZMKrvaLX64wkdu0+CBl03X6ZS
// SIG // nxaN6hySCURu9W3rWHww6PlpjSNzCxJvR6muORH4KrGb
// SIG // sBrDjutZlgCtzgxNstAxpghcKnr84nodV0yoZRjpeUBi
// SIG // JZZux8c3aoMhCI5B6t3ZVz8dd0mHKhYGXqY4aiISo1EZ
// SIG // g362MIIEozCCA4ugAwIBAgIQDs/0OMj+vzVuBNhqmBsa
// SIG // UDANBgkqhkiG9w0BAQUFADBeMQswCQYDVQQGEwJVUzEd
// SIG // MBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAu
// SIG // BgNVBAMTJ1N5bWFudGVjIFRpbWUgU3RhbXBpbmcgU2Vy
// SIG // dmljZXMgQ0EgLSBHMjAeFw0xMjEwMTgwMDAwMDBaFw0y
// SIG // MDEyMjkyMzU5NTlaMGIxCzAJBgNVBAYTAlVTMR0wGwYD
// SIG // VQQKExRTeW1hbnRlYyBDb3Jwb3JhdGlvbjE0MDIGA1UE
// SIG // AxMrU3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNl
// SIG // cyBTaWduZXIgLSBHNDCCASIwDQYJKoZIhvcNAQEBBQAD
// SIG // ggEPADCCAQoCggEBAKJjCzlEuLsjp0RJuw7/ofBhClOT
// SIG // sJjbrSwPSsVu/4Y8U1UPFc4EPyv9qZaW2b5heQtbyUyG
// SIG // duXgQ0sile7CK0PBn9hotI5AT+6FOLkRxSPyZFjwFTJv
// SIG // TlehroikAtcqHs1L4d1j1ReJMluwXplaqJ0oUA4X7pbb
// SIG // YTtFUR3PElYLkkf8q672Zj1HrHBy55LnX80QucSDZJQZ
// SIG // vSWA4ejSIqXQugJ6oXeTW2XD7hd0vEGGKtwITIySjJEt
// SIG // nndEH2jWqHR32w5bMotWizO92WPISZ06xcXqMwvS8aMb
// SIG // 9Iu+2bNXizveBKd6IrIkri7HcMW+ToMmCPsLvalPmQjh
// SIG // EChyqs0CAwEAAaOCAVcwggFTMAwGA1UdEwEB/wQCMAAw
// SIG // FgYDVR0lAQH/BAwwCgYIKwYBBQUHAwgwDgYDVR0PAQH/
// SIG // BAQDAgeAMHMGCCsGAQUFBwEBBGcwZTAqBggrBgEFBQcw
// SIG // AYYeaHR0cDovL3RzLW9jc3Aud3Muc3ltYW50ZWMuY29t
// SIG // MDcGCCsGAQUFBzAChitodHRwOi8vdHMtYWlhLndzLnN5
// SIG // bWFudGVjLmNvbS90c3MtY2EtZzIuY2VyMDwGA1UdHwQ1
// SIG // MDMwMaAvoC2GK2h0dHA6Ly90cy1jcmwud3Muc3ltYW50
// SIG // ZWMuY29tL3Rzcy1jYS1nMi5jcmwwKAYDVR0RBCEwH6Qd
// SIG // MBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0yMDQ4LTIwHQYD
// SIG // VR0OBBYEFEbGaaMOShQe1UzaUmMXP142vA3mMB8GA1Ud
// SIG // IwQYMBaAFF+a9W5czMx0mtTdfe8/2+xMgC7dMA0GCSqG
// SIG // SIb3DQEBBQUAA4IBAQB4O7SRKgBM8I9iMDd4o4QnB28Y
// SIG // st4l3KDUlAOqhk4ln5pAAxzdzuN5yyFoBtq2MrRtv/Qs
// SIG // JmMz5ElkbQ3mw2cO9wWkNWx8iRbG6bLfsundIMZxD82V
// SIG // dNy2XN69Nx9DeOZ4tc0oBCCjqvFLxIgpkQ6A0RH83Vx2
// SIG // bk9eDkVGQW4NsOo4mrE62glxEPwcebSAe6xp9P2ctgwW
// SIG // K/F/Wwk9m1viFsoTgW0ALjgNqCmPLOGy9FqpAa8VnCwv
// SIG // SRvbIrvD/niUUcOGsYKIXfA9tFGheTMrLnu53CAJE3Hr
// SIG // ahlbz+ilMFcsiUk/uc9/yb8+ImhjU5q9aXSsxR08f5Lg
// SIG // w7wc2AR1MIIFdDCCBFygAwIBAgIQJ2buVutJ846r13Ci
// SIG // /ITeIjANBgkqhkiG9w0BAQwFADBvMQswCQYDVQQGEwJT
// SIG // RTEUMBIGA1UEChMLQWRkVHJ1c3QgQUIxJjAkBgNVBAsT
// SIG // HUFkZFRydXN0IEV4dGVybmFsIFRUUCBOZXR3b3JrMSIw
// SIG // IAYDVQQDExlBZGRUcnVzdCBFeHRlcm5hbCBDQSBSb290
// SIG // MB4XDTAwMDUzMDEwNDgzOFoXDTIwMDUzMDEwNDgzOFow
// SIG // gYUxCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVy
// SIG // IE1hbmNoZXN0ZXIxEDAOBgNVBAcTB1NhbGZvcmQxGjAY
// SIG // BgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMSswKQYDVQQD
// SIG // EyJDT01PRE8gUlNBIENlcnRpZmljYXRpb24gQXV0aG9y
// SIG // aXR5MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKC
// SIG // AgEAkehUktIKVrGsDSTdxc9EZ3SZKzejfSNwAHG8U9/E
// SIG // +ioSj0t/EFa9n3Byt2F/yUsPF6c947AEYe7/EZfH9IY+
// SIG // Cvo+XPmT5jR62RRr55yzhaCCenavcZDX7P0N+pxs+t+w
// SIG // gvQUfvm+xKYvT3+Zf7X8Z0NyvQwA1onrayzT7Y+YHBSr
// SIG // fuXjbvzYqOSSJNpDa2K4Vf3qwbxstovzDo2a5JtsaZn4
// SIG // eEgwRdWt4Q08RWD8MpZRJ7xnw8outmvqRsfHIKCxH2Xe
// SIG // SAi6pE6p8oNGN4Tr6MyBSENnTnIqm1y9TBsoilwie7Sr
// SIG // mNnu4FGDwwlGTm0+mfqVF9p8M1dBPI1R7Qu2XK8sYxrf
// SIG // V8g/vOldxJuvRZnio1oktLqpVj3Pb6r/SVi+8Kj/9Lit
// SIG // 6Tf7urj0Czr56ENCHonYhMsT8dm74YlguIwoVqwUHZwK
// SIG // 53Hrzw7dPamWoUi9PPevtQ0iTMARgexWO/bTouJbt7IE
// SIG // IlKVgJNp6I5MZfGRAy1wdALqi2cVKWlSArvX31BqVUa/
// SIG // oKMoYX9w0MOiqiwhqkfOKJwGRXa/ghgntNWutMtQ5mv0
// SIG // TIZxMOmm3xaG4Nj/QN370EKIf6MzOi5cHkERgWPOGHFr
// SIG // K+ymircxXDpqR+DDeVnWIBqv8mqYqnK8V0rSS527EPyw
// SIG // TEHl7R09XiidnMy/s1Hap0flhFMCAwEAAaOB9DCB8TAf
// SIG // BgNVHSMEGDAWgBStvZh6NLQm9/rEJlTvA73gJMtUGjAd
// SIG // BgNVHQ4EFgQUu69+Aj36pvE8hI6t7jiY7NkyMtQwDgYD
// SIG // VR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wEQYD
// SIG // VR0gBAowCDAGBgRVHSAAMEQGA1UdHwQ9MDswOaA3oDWG
// SIG // M2h0dHA6Ly9jcmwudXNlcnRydXN0LmNvbS9BZGRUcnVz
// SIG // dEV4dGVybmFsQ0FSb290LmNybDA1BggrBgEFBQcBAQQp
// SIG // MCcwJQYIKwYBBQUHMAGGGWh0dHA6Ly9vY3NwLnVzZXJ0
// SIG // cnVzdC5jb20wDQYJKoZIhvcNAQEMBQADggEBAGS/g/Ff
// SIG // moXQzbihKVcN6Fr30ek+8nYEbvFScLsePP9NDXRqzIGC
// SIG // JdPDoCpdTPW6i6FtxFQJdcfjJw5dhHk3QBN39bSsHNA7
// SIG // qxcS1u80GH4r6XnTq1dFDK8o+tDb5VCViLvfhVdpfZLY
// SIG // Uspzgb8c8+a4bmYRBbMelC1/kZWSWfFMzqORcUx8Rww7
// SIG // Cxn2obFshj5cqsQugsv5B5a6SE2Q8pTIqXOi6wZ7I53e
// SIG // ovNNVZ96YUWYGGjHXkBrI/V5eu+MtWuLt29G9HvxPUsE
// SIG // 2JOAWVrgQSQdso8VYFhH2+9uRv0V9dlfmrPb2LjkQLPN
// SIG // lzmuhbsdjrzch5vRpu/xO28QOG8wggWUMIIEfKADAgEC
// SIG // AhBlgjjgUQxeM8mGNAX0oIMAMA0GCSqGSIb3DQEBCwUA
// SIG // MH0xCzAJBgNVBAYTAkdCMRswGQYDVQQIExJHcmVhdGVy
// SIG // IE1hbmNoZXN0ZXIxEDAOBgNVBAcTB1NhbGZvcmQxGjAY
// SIG // BgNVBAoTEUNPTU9ETyBDQSBMaW1pdGVkMSMwIQYDVQQD
// SIG // ExpDT01PRE8gUlNBIENvZGUgU2lnbmluZyBDQTAeFw0x
// SIG // NTAzMDUwMDAwMDBaFw0yMDAzMDQyMzU5NTlaMIHVMQsw
// SIG // CQYDVQQGEwJVUzEOMAwGA1UEEQwFNDQxNDMxDTALBgNV
// SIG // BAgMBE9oaW8xGTAXBgNVBAcMEE1heWZpZWxkIFZpbGxh
// SIG // Z2UxEDAOBgNVBAkMB1N1aXRlIEExGjAYBgNVBAkMETc2
// SIG // NyBCZXRhIERyIFN1aXRlMSIwIAYDVQQKDBlQcmVFbXB0
// SIG // aXZlIFNvbHV0aW9ucywgTExDMRYwFAYDVQQLDA1JVCBE
// SIG // ZXBhcnRtZW50MSIwIAYDVQQDDBlQcmVFbXB0aXZlIFNv
// SIG // bHV0aW9ucywgTExDMIIBIjANBgkqhkiG9w0BAQEFAAOC
// SIG // AQ8AMIIBCgKCAQEAwMZa8+Dx+dnhNS8z1XbvT6Y658U+
// SIG // MM2chmoVLrajpTE6wAnfoE4hGmziZhU4ONBwbbnlTsuJ
// SIG // GPfWTQGXdRr4bMWy0xCx440+Cof5bzyj3guhh9mtZlRD
// SIG // 1mAVXW0LLU+ntc0xsWf/UpxXSMnISmfJ1YzanR6f1qy4
// SIG // QHxYHms1fk/+gIv5bbh4Gy0z4QR+tix5Bb2vLNYjjHUI
// SIG // y0/MeRAqvbg33QjOJId229gJHnHRnOwsdLgTIkIzwOpZ
// SIG // PZmpI84NTs+ioCNshDxHz+bP+/KTC7F0sJzKlnieLTNJ
// SIG // 191IiJ9oBmqfT64QeKfqFTtsAZmucBbGVn6eRCybUGNT
// SIG // y1jU1wIDAQABo4IBtTCCAbEwHwYDVR0jBBgwFoAUKZFg
// SIG // /4pN+uv5pmq4z/nmS71JzhIwHQYDVR0OBBYEFEUIRPh2
// SIG // MN0/cJZwwts1Yq5NluSMMA4GA1UdDwEB/wQEAwIHgDAM
// SIG // BgNVHRMBAf8EAjAAMBMGA1UdJQQMMAoGCCsGAQUFBwMD
// SIG // MBEGCWCGSAGG+EIBAQQEAwIEEDBGBgNVHSAEPzA9MDsG
// SIG // DCsGAQQBsjEBAgEDAjArMCkGCCsGAQUFBwIBFh1odHRw
// SIG // czovL3NlY3VyZS5jb21vZG8ubmV0L0NQUzBDBgNVHR8E
// SIG // PDA6MDigNqA0hjJodHRwOi8vY3JsLmNvbW9kb2NhLmNv
// SIG // bS9DT01PRE9SU0FDb2RlU2lnbmluZ0NBLmNybDB0Bggr
// SIG // BgEFBQcBAQRoMGYwPgYIKwYBBQUHMAKGMmh0dHA6Ly9j
// SIG // cnQuY29tb2RvY2EuY29tL0NPTU9ET1JTQUNvZGVTaWdu
// SIG // aW5nQ0EuY3J0MCQGCCsGAQUFBzABhhhodHRwOi8vb2Nz
// SIG // cC5jb21vZG9jYS5jb20wJgYDVR0RBB8wHYEbaXRkZXBh
// SIG // cnRtZW50QHByZWVtcHRpdmUuY29tMA0GCSqGSIb3DQEB
// SIG // CwUAA4IBAQBfLY7UaFKILi4pr9mFTzccglyyJDuCsmY+
// SIG // v+R6RE66B3ffqIOcP1AXafK/tSpE8mfiyK/14NNjK1fT
// SIG // p6b8RY4aXv1s1/RitkPbEoYcwyVxGofDIGOHwGsaiy/F
// SIG // 4XV6P0TnQdKkVcw7NTMIHWmcpzDlKZDZ6haPWZzF61T6
// SIG // qRGDiaxjhjdazNWbU7CryMz8oNlnwkYxdVia+F2wxR05
// SIG // CMPQhm/+i8f7Tp79vNTqswPbkugu5lMsyX0TKARnhIQA
// SIG // AICIZx68rhRDlhTgo2gpMlVrUXu5PNlyRFwYD7Ur+yj/
// SIG // rCC0hK1IJowlKq47ucWOeN0ZA5/cGcrCkaeKCzS+Ur2V
// SIG // MIIF4DCCA8igAwIBAgIQLnyHzA6TSlL+lP0ct800rzAN
// SIG // BgkqhkiG9w0BAQwFADCBhTELMAkGA1UEBhMCR0IxGzAZ
// SIG // BgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4GA1UE
// SIG // BxMHU2FsZm9yZDEaMBgGA1UEChMRQ09NT0RPIENBIExp
// SIG // bWl0ZWQxKzApBgNVBAMTIkNPTU9ETyBSU0EgQ2VydGlm
// SIG // aWNhdGlvbiBBdXRob3JpdHkwHhcNMTMwNTA5MDAwMDAw
// SIG // WhcNMjgwNTA4MjM1OTU5WjB9MQswCQYDVQQGEwJHQjEb
// SIG // MBkGA1UECBMSR3JlYXRlciBNYW5jaGVzdGVyMRAwDgYD
// SIG // VQQHEwdTYWxmb3JkMRowGAYDVQQKExFDT01PRE8gQ0Eg
// SIG // TGltaXRlZDEjMCEGA1UEAxMaQ09NT0RPIFJTQSBDb2Rl
// SIG // IFNpZ25pbmcgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IB
// SIG // DwAwggEKAoIBAQCmmJBjd5E0f4rR3elnMRHrzB79MR2z
// SIG // uWJXP5O8W+OfHiQyESdrvFGRp8+eniWzX4GoGA8dHiAw
// SIG // Dvthe4YJs+P9omidHCydv3Lj5HWg5TUjjsmK7hoMZMfY
// SIG // QqF7tVIDSzqwjiNLS2PgIpQ3e9V5kAoUGFEs5v7BEvAc
// SIG // P2FhCoyi3PbDMKrNKBh1SMF5WgjNu4xVjPfUdpA6M0ZQ
// SIG // c5hc9IVKaw+A3V7Wvf2pL8Al9fl4141fEMJEVTyQPDFG
// SIG // y3CuB6kK46/BAW+QGiPiXzjbxghdR7ODQfAuADcUuRKq
// SIG // eZJSzYcPe9hiKaR+ML0btYxytEjy4+gh+V5MYnmLAgaf
// SIG // f9ULAgMBAAGjggFRMIIBTTAfBgNVHSMEGDAWgBS7r34C
// SIG // Pfqm8TyEjq3uOJjs2TIy1DAdBgNVHQ4EFgQUKZFg/4pN
// SIG // +uv5pmq4z/nmS71JzhIwDgYDVR0PAQH/BAQDAgGGMBIG
// SIG // A1UdEwEB/wQIMAYBAf8CAQAwEwYDVR0lBAwwCgYIKwYB
// SIG // BQUHAwMwEQYDVR0gBAowCDAGBgRVHSAAMEwGA1UdHwRF
// SIG // MEMwQaA/oD2GO2h0dHA6Ly9jcmwuY29tb2RvY2EuY29t
// SIG // L0NPTU9ET1JTQUNlcnRpZmljYXRpb25BdXRob3JpdHku
// SIG // Y3JsMHEGCCsGAQUFBwEBBGUwYzA7BggrBgEFBQcwAoYv
// SIG // aHR0cDovL2NydC5jb21vZG9jYS5jb20vQ09NT0RPUlNB
// SIG // QWRkVHJ1c3RDQS5jcnQwJAYIKwYBBQUHMAGGGGh0dHA6
// SIG // Ly9vY3NwLmNvbW9kb2NhLmNvbTANBgkqhkiG9w0BAQwF
// SIG // AAOCAgEAAj8COcPu+Mo7id4MbU2x8U6ST6/COCwEzMVj
// SIG // EasJY6+rotcCP8xvGcM91hoIlP8l2KmIpysQGuCbsQci
// SIG // GlEcOtTh6Qm/5iR0rx57FjFuI+9UUS1SAuJ1CAVM8bdR
// SIG // 4VEAxof2bO4QRHZXavHfWGshqknUfDdOvf+2dVRAGDZX
// SIG // ZxHNTwLk/vPa/HUX2+y392UJI0kfQ1eD6n4gd2HITfK7
// SIG // ZU2o94VFB696aSdlkClAi997OlE5jKgfcHmtbUIgos8M
// SIG // bAOMTM1zB5TnWo46BLqioXwfy2M6FafUFRunUkcyqfS/
// SIG // ZEfRqh9TTjIwc8Jvt3iCnVz/RrtrIh2IC/gbqjSm/Iz1
// SIG // 3X9ljIwxVzHQNuxHoc/Li6jvHBhYxQZ3ykubUa9MCEp6
// SIG // j+KjUuKOjswm5LLY5TjCqO3GgZw1a6lYYUoKl7RLQrZV
// SIG // nb6Z53BtWfhtKgx/GWBfDJqIbDCsUgmQFhv/K53b0CDK
// SIG // ieoofjKOGd97SDMe12X4rsn4gxSTdn1k0I7OvjV9/3Ix
// SIG // TZ+evR5sL6iPDAZQ+4wns3bJ9ObXwzTijIchhmH+v1V0
// SIG // 4SF3AwpobLvkyanmz1kl63zsRQ55ZmjoIs2475iFTZYR
// SIG // PAmK0H+8KCgT+2rKVI2SXM3CZZgGns5IW9S1N5NGQXwH
// SIG // 3c/6Q++6Z2H/fUnguzB9XIDj5hY5S6cxggSFMIIEgQIB
// SIG // ATCBkTB9MQswCQYDVQQGEwJHQjEbMBkGA1UECBMSR3Jl
// SIG // YXRlciBNYW5jaGVzdGVyMRAwDgYDVQQHEwdTYWxmb3Jk
// SIG // MRowGAYDVQQKExFDT01PRE8gQ0EgTGltaXRlZDEjMCEG
// SIG // A1UEAxMaQ09NT0RPIFJTQSBDb2RlIFNpZ25pbmcgQ0EC
// SIG // EGWCOOBRDF4zyYY0BfSggwAwCQYFKw4DAhoFAKCBujAZ
// SIG // BgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgorBgEE
// SIG // AYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0B
// SIG // CQQxFgQUAaHicaceNurpSPpebckWVkdc9tYwWgYKKwYB
// SIG // BAGCNwIBDDFMMEqgKoAoAFAAcgBlAEUAbQBwAHQAaQB2
// SIG // AGUAIABTAG8AbAB1AHQAaQBvAG4Ac6EcgBpodHRwOi8v
// SIG // d3d3LnByZWVtcHRpdmUuY29tIDANBgkqhkiG9w0BAQEF
// SIG // AASCAQBOxf6qGnrl3Q0BJw5h8whPKJ1n7gZWqBRdPO1A
// SIG // l6JiSOGzs8vTNrL8nLw/6J3XB3VEe2AVhy9Kge7H4WLl
// SIG // ODpgGtTnL7KDwWGCtyJgE0B6AyKScibRu2ps/6q5vzT8
// SIG // FzvsLDvU0dAlQm43UqZAh0xOT+sT3gbJRI6BLLQQ8rbG
// SIG // 5eevTFdGb+q52Fj274LXVhe1ZrgtG4f+7iDZIyjsna2W
// SIG // r28lAmFgc2H9vToDfg5YweJFO0cebqV4KCjUYFeWdVa2
// SIG // SwrgICPebki+UOY4Pk6iBqkfCrMQcFTh/pe9Eq78Gavf
// SIG // dQH3cnUk2HTFeTBJg1uRcMloAS99zMyIqsAMkdbmoYIC
// SIG // CzCCAgcGCSqGSIb3DQEJBjGCAfgwggH0AgEBMHIwXjEL
// SIG // MAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENv
// SIG // cnBvcmF0aW9uMTAwLgYDVQQDEydTeW1hbnRlYyBUaW1l
// SIG // IFN0YW1waW5nIFNlcnZpY2VzIENBIC0gRzICEA7P9DjI
// SIG // /r81bgTYapgbGlAwCQYFKw4DAhoFAKBdMBgGCSqGSIb3
// SIG // DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8X
// SIG // DTE2MDUxMjIwNDU0OFowIwYJKoZIhvcNAQkEMRYEFMZU
// SIG // /lssw+JHK5LeENXU9e6Qc6JkMA0GCSqGSIb3DQEBAQUA
// SIG // BIIBAJFbdWSWOWiHelnZhdPwOU/MCTQZABXE1kH0sNcq
// SIG // 3vd5ql9dxKfRhQZliLvDpRi/dmz4i49MJFWOFD+sbHlA
// SIG // GwRty5NxEamGVFtb3g7as5SbvozlQwP9EiwaKknoNTKd
// SIG // gVynHxNfj7NVoOPUnFeDs9I//h5JveIQmkpVkHwgapve
// SIG // NrqUyoxPoOQJ5QUzYAc6k6dkfO6EDWmb824lcpbLnNh6
// SIG // jfihe/tAKBKCdBe1Sa7e15lMBwaebfqLAzWKuxY4xMNh
// SIG // DCD9BgDqogzBqMkBTllGgv18ViCU1qSKcX/RSbGYBYOg
// SIG // 5EmDGIAjxcgLl0Y6FZh+d3lKB9Wa82A3i2Kv6sw=
// SIG // End signature block
