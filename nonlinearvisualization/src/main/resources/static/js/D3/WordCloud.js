// This function will be called after the entire document has
// finished loading.
window.onload = function(){
  
    WordCloud.create();
};

var WordCloud = {

//****************************************************************************
// Private methods.
//****************************************************************************
init:function()
{        
    $("#btnCreate")
        .on("click", this.onCreateBtnClick);
    $("#btnSave")
        .on("click", this.onSendBtnClick);    
},

readTXTFile: function(onTextRead)
{
    if (!window.File || !window.FileReader || !window.FileList || !window.Blob)
    { 
      return -1;
    }
    else
    {       
        var input = document.getElementById("txtFile");
        var txtFile = input.files[0];
        var reader = new FileReader();
        reader.onload = function(e){
            var txtText = e.target.result;
            var expression = /[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\?|\n|\t]/g;
            txtText = txtText.replace(expression,",");
            var txtArray = txtText.split(",");
            onTextRead(txtArray);
        }
        reader.readAsText(txtFile); 
        return 1;
    }
},

generateWordList: function(array)
{
    var length = array.length;
    var ArrayWithUniqueValues = [];
    var objectCounter = {};
    for (i = 0; i < length; i++) 
    {
        var currentMemberOfArrayKey = array[i];
        var currentMemberOfArrayValue = array[i];
        if (objectCounter[currentMemberOfArrayKey] === undefined)
        {
            ArrayWithUniqueValues.push(currentMemberOfArrayValue);
            objectCounter[currentMemberOfArrayKey] = 1;
        }
        else
        {
            objectCounter[currentMemberOfArrayKey]++;
        }
    }
    var w = WordCloud;
    var newArray = w.cutoffWordList(objectCounter, 20);
    console.log(objectCounter);
    return objectCounter;
},

cutoffWordList: function(obj, limitNumber)
{
    //convert object into array
    var sortable=[];
    for(var key in obj)
    {
        if(obj.hasOwnProperty(key))
        {
            sortable.push([key, obj[key]]); // each item is an array in format [key, value]
        }
    }        
    //sort items by value
    sortable.sort(function(a, b)
    {
    return b[1] - a[1]; //compare numbers, return array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    });     
    var length = sortable.length;
    if(limitNumber < length)
    {
        sortable = sortable.slice(0, limitNumber);
    }     
    var newObj = {};
    for(var ii = 0; ii < sortable.length; ii++)
    {
        newObj[sortable[ii][0]] = sortable[ii][1];
    } 
    console.log(newObj);
    return newObj; 
},

drawWordCloud: function(obj, sizeFactor, colormap, fontFamily, svgBoxWidth, svgBoxHeight)
{
    var arrWord = Object.keys(obj);
    var arrWeight = Object.values(obj);
  
    var width = svgBoxWidth;
    var height = svgBoxHeight;
         
    var fill = d3.scale.category20();
    
    var layout = d3.layout.cloud()
                .size([width, height])
                .words(
                    arrWord.map(function(d) {
                        var index = arrWord.indexOf(d);
                        return {text: d, size: arrWeight[index] * sizeFactor};
                    })
                )
                .padding(5)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font(fontFamily)
                .fontSize(function(d) { return d.size; })
                .spiral("rectangular")
                .on("end", draw)
                .start();
                
    function draw(words)
    {
        var width = svgBoxWidth;
        var height = svgBoxHeight;
        d3.select("#svgBox").append("svg")  
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
            .selectAll("text")
            .data(words)
            .enter().append("text")
            .style("font-size", function(d) { return d.size + "px"; })
            .style("font-family", fontFamily)
            .style("fill", function(d, i) { return fill(i); })
            .attr("text-anchor", "middle")
            // without the transform, words would get cutoff to the left and top, they would
            // appear outside of the SVG area
            .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) { return d.text; });
    }                
},

enableSaveButton: function()
{
    if($("#svgBox svg").length == 0)
    {
        $("#btnSave").prop("disabled", true);
    }
    else
    {
        $("#btnSave").prop("disabled", false);
    }
},

saveSVGToPNG: function()
{
    var svg = document.querySelector("svg");
    var svgData = new XMLSerializer().serializeToString(svg);
    var svgSize = svg.getBoundingClientRect();
    var canvas = document.createElement("canvas");
    canvas.width = svgSize.width;
    canvas.height = svgSize.height;
    var ctx = canvas.getContext("2d");
    var img = document.createElement("img");
    img.setAttribute('crossOrigin', 'anonymous');
    img.setAttribute("src", "data:image/svg+xml;base64," + btoa(svgData));
    img.onload = function() {
        ctx.drawImage(img, 0, 0);
        var imgDataURL = canvas.toDataURL("image/png");
        console.log(canvas.toDataURL("image/png"));
        $("body").append("<a id='hiddenLink' href='" + imgDataURL + "' style='display:none;' download>Download Pic</a>");
        $("#hiddenLink")[0].click();
        $("#hiddenLink").remove();
    };

},

//Click Create button to create word cloud svg file
onCreateBtnClick: function(event)
{
    alert("onCreateBtnClick事件触发了！")

    var w = WordCloud;
    var limitNumber = 100;
    var colormap = "Spectral";
    var fontFamily = "Courier New";
    var sizeFactor = 10;
    var width = 600;
    var height = 400;

    $("#svgBox svg").remove();

    txtArray = wordCloud;
    var txtObj = w.generateWordList(txtArray);
    //var w_TXTOBJ = w.cutoffWordList(txtObj,limitNumber); // Used to set the number of words, if you need, uncomment this line
    w.drawWordCloud(txtObj, sizeFactor, colormap, fontFamily, width, height);
    w.enableSaveButton();
},

//Click Save button to save svg file as png file
//Only work in Chrome and Firefox, not work in IE so far
onSendBtnClick: function(event)
{
    var w = WordCloud;
    w.saveSVGToPNG();
},


//****************************************************************************
// Public methods.
//****************************************************************************
create: function()
{
  WordCloud.init();  
},

}; //End var WordCloud
