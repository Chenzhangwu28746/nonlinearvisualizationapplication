function Vec3(data) {
    this.x = 0;
    this.y = 0;
    this.z = 0;
    
    if(!(typeof data === 'undefined')) {
        if(data instanceof Vec3) {
            this.x = data.x;
            this.y = data.y;
            this.z = data.z;
        } else {
            this.x = data[0];
            this.y = data[1];
            this.z = data[2];
        }
    }
    
    this.set = function(v) { this.x = v.x; this.y = v.y; this.z = v.z; return this; }
    this.setxyz = function(x,y,z) { this.x = x; this.y = y; this.z = z; return this; }
    
    this.len = function() { return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z); };
    this.lenSqr = function() { return this.x*this.x + this.y*this.y + this.z*this.z; };
    
    this.normalize = function() {
        return this.scale(1.0 / this.len());
    };
    
    this.scale = function(s) {
        this.x *= s;
        this.y *= s;
        this.z *= s;
        return this;
    };
    
    this.add = function(v) {
        this.x += v.x;
        this.y += v.y;
        this.z += v.z;
        return this;
    };
    
    this.sub = function(v) {
        this.x -= v.x;
        this.y -= v.y;
        this.z -= v.z;
        return this;
    };
    
    this.negate = function() {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
        return this;
    };
    this.toString = function() {
        return '<' + this.x + ',' + this.y + ',' + this.z + '>';
    }
}

Vec3.scale = function(v, s) {
    return (new Vec3(v)).scale(s);
}

Vec3.normalize = function(v) {
    return (new Vec3(v)).normalize();
}

Vec3.add = function(a,b) {
    return (new Vec3(a)).add(b);
}

Vec3.sub = function(a,b) {
    return (new Vec3(a)).sub(b);
};

Vec3.dist = function(a,b) {
    dx = a.x - b.x;
    dy = a.y - b.y;
    dz = a.z - d.z;
    return Math.sqrt(dx*dx + dy*dy + dz*dz);
}

Vec3.len = function(v) {
    return Math.sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

Vec3.dot = function(a,b) {
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

Vec3.cross = function(a,b) {
    // a.y*b.z-a.z*b.y,a.z*b.x-a.x*b.z,a.x*b.y-a.y*b.x
    return new Vec3([
        a.y*b.z - a.z*b.y,
        a.z*b.x - a.x*b.z,
        a.x*b.y - a.y*b.x
        ]);
}



