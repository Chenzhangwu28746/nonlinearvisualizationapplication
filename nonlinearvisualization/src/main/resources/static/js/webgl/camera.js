function Camera() {
    this.fx = new Vec3([1,0,0]);
    this.fy = new Vec3([0,0,1]);
    this.fz = new Vec3([0,-1,0]);
    this.fo = new Vec3([0,0,0]);
    this.fd = 2;
    
    this.lookat = function(eye, center, up) {
        this.fo = eye;
        this.fz = Vec3.sub(center, eye).normalize();
        this.fx = Vec3.cross(up, this.fz).normalize();
        this.fy = Vec3.cross(this.fz, this.fx).normalize();
        this.fd = Vec3.dist(eye, center);
        this.fz.negate();
    };
    
    this.rotate_axisangle = function(axis, deg) {
        deg *= 0.5;
        rad = deg * 3.14159 / 180.0;
        c = Math.cos(rad);
        s = Math.sin(rad);
        scale = Vec3.scale;
        cross = Vec3.cross;
        dot   = Vec3.dot;
        this.fx = scale(this.fx, c).add(scale(cross(axis,this.fx), s)).add(scale(scale(axis, dot(axis, this.fx)), 1.0-c)).normalize();
        this.fy = scale(this.fy, c).add(scale(cross(axis,this.fy), s)).add(scale(scale(axis, dot(axis, this.fy)), 1.0-c)).normalize();
        this.fz = scale(this.fz, c).add(scale(cross(axis,this.fz), s)).add(scale(scale(axis, dot(axis, this.fz)), 1.0-c)).normalize();
        return this;
    };
    
    this.dolly = function(dl) {
        this.fd *= dl;
        return this;
    };
    
    this.pan = function(px, py) {
        scale = this.fd * 0.002;
        this.fo.add(Vec3.scale(this.fx, px * scale)).add(Vec3.scale(this.fy, py * scale));
        return this;
    };
    
    this.viewmatrix = function() {
        r = this.fx;
        u = this.fy;
        b = this.fz;
        o = this.fo;
        d = -this.fd;
        rot = new Matrix4().setValues(r.x,u.x,b.x,0, r.y,u.y,b.y,0, r.z,u.z,b.z,0, 0,0,0,1);
        tra = new Matrix4().setValues(1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,d,1);
        tra2 = new Matrix4().setValues(1,0,0,0, 0,1,0,0, 0,0,1,0, -o.x,-o.y,-o.z,1);
        tra.concat(rot).concat(tra2);
        return tra.elements;
    };
};

